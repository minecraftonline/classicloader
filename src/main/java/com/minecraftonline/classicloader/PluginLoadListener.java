/*
 * Copyright (C) 2013 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.classicloader;

import net.canarymod.hook.HookHandler;
import net.canarymod.hook.system.LoadWorldHook;
import net.canarymod.plugin.PluginListener;
import net.canarymod.plugin.Priority;

import com.minecraftonline.classicloader.classicapi.etc;

/**
 * ClassicLoader's plugin loading related listener
 * @author Willem Mulder
 */
public class PluginLoadListener implements PluginListener {
    private boolean loaded = false;

    @HookHandler(ignoreCanceled = true, priority = Priority.PASSIVE)
    public void handleWorldLoad(LoadWorldHook worldLoad) {
        if (!loaded) {
            etc.getLoader().loadPlugins();
            loaded = true;
        }
    }
}
