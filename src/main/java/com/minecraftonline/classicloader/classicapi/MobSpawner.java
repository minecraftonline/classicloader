package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.CanaryMobSpawnerLogic;
import com.minecraftonline.classicloader.Util;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.tileentity.MobSpawnerBaseLogic;
import net.minecraft.tileentity.TileEntityMobSpawner;

/**
 * MobSpawner.java - Wrapper for mob spawners.
 *
 * @author James
 */
@RecodeProxy(net.canarymod.api.world.blocks.MobSpawner.class)
public class MobSpawner extends MobSpawnerLogic implements ComplexBlock {

    private net.canarymod.api.world.blocks.MobSpawner spawner;

    /**
     * Creates an interface for the spawner.
     *
     * @param spawner
     */
    public MobSpawner(TileEntityMobSpawner spawner) {
        // SRG super(spawner.func_145881_a());
        super(spawner.a());
        this.spawner = spawner.getCanaryMobSpawner();
    }

    public MobSpawner(net.canarymod.api.world.blocks.MobSpawner spawner) {
        super(Util.getField(CanaryMobSpawnerLogic.class, spawner.getLogic(), "logic", MobSpawnerBaseLogic.class));
        this.spawner = spawner;
    }

    @Override
    public Block getBlock() {
        return getWorld().getBlockAt(getX(), getY(), getZ());
    }

    /**
     * Reads the data for this spawner from an NBTTagCompound
     *
     * @param tag the tag to read from
     */
    @Override
    public void readFromTag(NBTTagCompound tag) {
        spawner.readFromTag(tag.getRecodeHandle());
    }

    /**
     * Writes the data for this spawner to an NBTTagCompound
     *
     * @param tag the tag to write to
     */
    @Override
    public void writeToTag(NBTTagCompound tag) {
        spawner.writeToTag(tag.getRecodeHandle());
    }

    @Override
    public NBTTagCompound getMetaTag() {
        return Wrapper.wrap(spawner.getMetaTag());
    }

    @Override
    public void update() {
        spawner.update();
    }

    public net.canarymod.api.world.blocks.MobSpawner getRecodeHandle() {
        return spawner;
    }
}
