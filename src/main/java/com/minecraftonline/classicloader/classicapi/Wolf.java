package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.DyeColor;
import net.canarymod.api.entity.living.animal.CanaryWolf;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.passive.EntityWolf;

/**
 * Wrap around an EntityWolf to provide extra methods not available to anything
 * but wolves.
 *
 * @author Brian McCarthy
 *
 */
@RecodeProxy(net.canarymod.api.entity.living.animal.Wolf.class)
public class Wolf extends TamableEntity {

    /**
     * Basic wolf constructor.
     *
     * @param entity An instance of EntityWolf to wrap around.
     */
    public Wolf(EntityWolf entity) {
        super(entity);
    }

    public Wolf(net.canarymod.api.entity.living.animal.Wolf wolf) {
        super(wolf);
    }

    /**
     * If this wolf is angry.
     *
     * @return Boolean of if this wolf is angry.
     */
    public boolean isAngry() {
        return getRecodeHandle().isAngry();
    }

    /**
     * Sets if this wolf is angry of not.
     *
     * @param angry New angry state of the wolf.
     */
    public void setAngry(boolean angry) {
        getRecodeHandle().setAngry(angry);
    }

    /**
     * Gets this wolf's collar colour.
     *
     * @return collar colour as an int
     */
    public int getCollarColour() {
        return getRecodeHandle().getCollarColor().getDyeColorCode();
    }

    /**
     * Sets this wolf's collar colour.
     *
     * @param colour new collar colour
     */
    public void setCollarColour(int colour) {
        getRecodeHandle().setCollarColor(DyeColor.fromDyeColorCode(colour));
    }

    @Override
    public EntityWolf getEntity() {
        return (EntityWolf) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.living.animal.Wolf getRecodeHandle() {
        return (net.canarymod.api.entity.living.animal.Wolf) super.getRecodeHandle();
    }
}
