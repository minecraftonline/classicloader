package com.minecraftonline.classicloader.classicapi;

import java.util.Arrays;
import net.canarymod.Canary;
import net.canarymod.api.nbt.ByteArrayTag;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for the net.minecraft.server.NBTTagByteArray class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(ByteArrayTag.class)
public class NBTTagByteArray extends NBTBase {

    /**
     * Creates a wrapper around an net.minecraft.server.NBTTagByteArray
     *
     * @param baseTag
     */
    public NBTTagByteArray(net.minecraft.nbt.NBTTagByteArray baseTag) {
        super(baseTag);
    }

    /**
     * Creates a new NBTTagByteArray
     *
     * @param name the name of the tag
     */
    public NBTTagByteArray(String name) {
        this(name, null);
    }

    /**
     * Creates a new NBTTagByteArray
     *
     * @param name the name of the tag
     * @param value the value of the tag
     */
    public NBTTagByteArray(String name, byte[] value) {
        this(Canary.factory().getNBTFactory().newByteArrayTag(value));
    }

    public NBTTagByteArray(ByteArrayTag byteArrayTag) {
        super(byteArrayTag);
    }

    /**
     * Returns the value of this tag
     *
     * @return
     */
    public byte[] getValue() {
        return getRecodeHandle().getValue();
    }

    /**
     * Sets the value of this tag
     *
     * @param value the new value
     */
    public void setValue(byte[] value) {
        getRecodeHandle().setValue(value);
    }

    @Override
    public net.minecraft.nbt.NBTTagByteArray getBaseTag() {
        return (net.minecraft.nbt.NBTTagByteArray) super.getBaseTag();
    }

    @Override
    public String toString() {
        return String.format("NBTTag[type=%s, name=%s, value=%s]", getTagName(getType()), getName(), Arrays.toString(getValue()));
    }

    @Override
    public ByteArrayTag getRecodeHandle() {
        return (ByteArrayTag) super.getRecodeHandle();
    }

    public static NBTTagByteArray[] fromRecodeArray(ByteArrayTag[] recode) {
        NBTTagByteArray[] classic = new NBTTagByteArray[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new NBTTagByteArray(recode[i]);
        }
        return classic;
    }

    public static ByteArrayTag[] toRecodeArray(NBTTagByteArray[] classic) {
        ByteArrayTag[] recode = new ByteArrayTag[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
