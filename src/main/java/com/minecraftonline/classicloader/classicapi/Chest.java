package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.tileentity.TileEntityChest;

/**
 * Chest.java - Interface to chests.
 *
 * @author James
 */
@RecodeProxy(net.canarymod.api.world.blocks.Chest.class)
public class Chest extends BaseContainerBlock<TileEntityChest> {

    public Chest(TileEntityChest chest) {
        this(null, chest);
    }

    public Chest(net.minecraft.inventory.Container oContainer, TileEntityChest chest) {
        super(oContainer, chest, "Chest");
    }

    public Chest(net.canarymod.api.world.blocks.Chest chest) {
        super(chest, "Chest");
    }

    public DoubleChest findAttachedChest() {
        return Wrapper.wrap(getRecodeHandle().getDoubleChest());
    }

    @Override
    public net.canarymod.api.world.blocks.Chest getRecodeHandle() {
        return tileEntity.getCanaryChest();
    }
}
