package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.entity.vehicle.Vehicle;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * BaseVehicle - Base class for interfacing boats and minecarts
 *
 * @author James
 */
@RecodeProxy(Vehicle.class)
public class BaseVehicle extends BaseEntity {

    /**
     * Creates an interface for a vehicle
     *
     * @param entity
     */
    public BaseVehicle(net.minecraft.entity.Entity entity) {
        super(entity);
    }

    /**
     * Interface for vehicles.
     */
    public BaseVehicle() {
    }

    public BaseVehicle(Vehicle vehicle) {
        super(vehicle);
    }

    /**
     * Checks if this vehicle is empty (unoccupied)
     *
     * @return true if unoccupied.
     */
    public boolean isEmpty() {
        return getRecodeHandle().isEmpty();
    }

    /**
     * Returns the passenger. If there is no passenger this function returns
     * null.
     *
     * @return passenger
     */
    public Player getPassenger() {
        if (this.getRiddenByEntity() != null) {
            return getRiddenByEntity().getPlayer();
        }

        return null;
    }

    /**
     * Convenience method for setting a player in a minecart because we have
     * getPassenger()
     *
     * @param p
     */
    public void setPassenger(Player p) {
        setRiddenByEntity(p);
    }

    @Override
    public Vehicle getRecodeHandle() {
        return (Vehicle) super.getRecodeHandle();
    }

    public static BaseVehicle[] fromRecodeArray(Vehicle[] recode) {
        BaseVehicle[] classic = new BaseVehicle[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = Wrapper.wrap(recode[i]);
        }
        return classic;
    }

    public static Vehicle[] toRecodeArray(BaseVehicle[] classic) {
        Vehicle[] recode = new Vehicle[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
