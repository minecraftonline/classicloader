package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.CanaryVillagerTrade;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.village.MerchantRecipe;

/**
 * Interface for the MerchantRecipe class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.VillagerTrade.class)
public class VillagerTrade {

    private final net.canarymod.api.VillagerTrade trade;

    /**
     * Wraps an MerchantRecipe
     *
     * @param recipeBase the recipe to wrap
     */
    public VillagerTrade(MerchantRecipe recipe) {
        this(new CanaryVillagerTrade(recipe));
    }

    /**
     * Creates a new villager trade
     *
     * @param buying the item the player will give to the villager
     * @param selling the item the villager will give to the player
     */
    public VillagerTrade(Item buying, Item selling) {
        this(Canary.factory().getObjectFactory().newVillagerTrade(buying.getRecodeHandle(), selling.getRecodeHandle()));
    }

    /**
     * Creates a new villager trade
     *
     * @param buyingOne the first item the player will give to the villager
     * @param buyingTwo the second item the player will give to the villager
     * @param selling the item the villager will give to the player
     */
    public VillagerTrade(Item buyingOne, Item buyingTwo, Item selling) {
        this(Canary.factory().getObjectFactory().newVillagerTrade(buyingOne.getRecodeHandle(), buyingTwo.getRecodeHandle(), selling.getRecodeHandle()));
    }

    /**
     * Creates a new villager trade
     *
     * @param tag the tag to read the trade data from
     */
    public VillagerTrade(NBTTagCompound tag) {
        this(new MerchantRecipe(tag.getBaseTag()));
    }

    public VillagerTrade(net.canarymod.api.VillagerTrade trade) {
        this.trade = trade;
    }

    /**
     * Returns the base recipe for this trade.
     *
     * @return
     */
    public MerchantRecipe getRecipe() {
        return ((CanaryVillagerTrade) trade).getRecipe();
    }

    /**
     * Returns the first item the player must give to the villager.
     *
     * @return
     */
    public Item getBuyingOne() {
        return Wrapper.wrap(trade.getBuyingOne());
    }

    /**
     * Sets the first item the player must give to the villager.
     *
     * @param item The item
     */
    public void setBuyingOne(Item item) {
        trade.setBuyingOne(item.getRecodeHandle());
    }

    /**
     * Returns the second item the player must give to the villager.
     *
     * @return
     */
    public Item getBuyingTwo() {
        return Wrapper.wrap(trade.getBuyingTwo());
    }

    /**
     * Sets the second item the player must give to the villager.
     *
     * @param item The item
     */
    public void setBuyingTwo(Item item) {
        trade.setBuyingTwo(item.getRecodeHandle());
    }

    /**
     * Returns whether or not this trade requires the player to give the
     * villager two items.
     *
     * @return true if the player must give two items, false if the player must
     * give only one
     */
    public boolean requiresTwoItems() {
        return trade.requiresTwoItems();
    }

    /**
     * Returns the item the player receives from the trade.
     *
     * @return
     */
    public Item getSelling() {
        return Wrapper.wrap(trade.getSelling());
    }

    /**
     * Sets the item the player receives from the trade.
     *
     * @param item
     */
    public void setSelling(Item item) {
        trade.setSelling(item.getRecodeHandle());
    }

    /**
     * Increase the number of times this was used by one.
     */
    public void use() {
        trade.use();
    }

    /**
     * Increases the maximum amount of times this trade can be used. The default
     * max is 7
     *
     * @param increase the amount to increase it buy
     */
    public void increaseMaxUses(int increase) {
        trade.increaseMaxUses(increase);
    }

    /**
     * Returns whether or not this recipe has exceeded its max usages and can no
     * longer be used.
     *
     * @return
     */
    public boolean isUsedUp() {
        return trade.isUsedUp();
    }

    /**
     * Returns the data for this trade in an NBTCompoundTag.
     *
     * @return
     */
    public NBTTagCompound getDataAsTag() {
        return new NBTTagCompound(trade.getDataAsTag());
    }

    /**
     * Reads the data from an NBTTagCompound into this trade
     *
     * @param tag the tag to read the data from
     */
    public void readFromTag(NBTTagCompound tag) {
        trade.readFromTag(tag.getRecodeHandle());
    }

    @Override
    public String toString() {
        return String.format("VillagerTrade[buying1=%s, buying2=%s, selling=%s]", getBuyingOne(), getBuyingTwo(), getSelling());
    }

    public net.canarymod.api.VillagerTrade getRecodeHandle() {
        return trade;
    }

    public static VillagerTrade[] fromRecodeArray(net.canarymod.api.VillagerTrade[] recode) {
        VillagerTrade[] classic = new VillagerTrade[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new VillagerTrade(recode[i]);
        }
        return classic;
    }

    public static net.canarymod.api.VillagerTrade[] toRecodeArray(VillagerTrade[] classic) {
        net.canarymod.api.VillagerTrade[] recode = new net.canarymod.api.VillagerTrade[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
