package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.world.blocks.CanaryDoubleChest;
import com.minecraftonline.classicloader.classicapi.bridge.ContainerProxy;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.inventory.InventoryLargeChest;

@RecodeProxy(net.canarymod.api.world.blocks.DoubleChest.class)
public class DoubleChest extends ItemArray<ContainerProxy> implements ComplexBlock, Inventory {

    private final net.canarymod.api.world.blocks.DoubleChest chest;
    private String name = "Large Chest";

    public DoubleChest(InventoryLargeChest chest) {
        this(null, chest);
    }

    public DoubleChest(net.minecraft.inventory.Container oContainer, InventoryLargeChest chest) {
        super(oContainer, ContainerProxy.of(oContainer.getInventory()));
        this.chest = new CanaryDoubleChest(chest);
    }

    public DoubleChest(net.canarymod.api.world.blocks.DoubleChest dchest) {
        super(ContainerProxy.of(dchest));
        this.chest = dchest;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String value) {
        name = value;
    }

    @Override
    public int getX() {
        return chest.getX();
    }

    @Override
    public int getY() {
        return chest.getY();
    }

    @Override
    public int getZ() {
        return chest.getZ();
    }

    @Override
    public void update() {
        chest.update();
    }

    @Override
    public Block getBlock() {
        return new Block(chest.getBlock());
    }

    @Override
    public World getWorld() {
        return Wrapper.wrap(chest.getWorld());
    }

    @Override
    public NBTTagCompound getMetaTag() {
        return new NBTTagCompound(chest.getMetaTag());
    }

    @Override
    public void writeToTag(NBTTagCompound tag) {
        chest.writeToTag(tag.getRecodeHandle());
    }

    @Override
    public void readFromTag(NBTTagCompound tag) {
        chest.readFromTag(tag.getRecodeHandle());
    }

    public net.canarymod.api.world.blocks.DoubleChest getRecodeHandle() {
        return chest;
    }
}
