package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.potion.PotionEffectType;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.tileentity.TileEntityBeacon;

/**
 * An interface class to Beacons.
 *
 * @author m4411k4
 */
@RecodeProxy(net.canarymod.api.world.blocks.Beacon.class)
public class Beacon extends BaseContainerBlock<TileEntityBeacon> {

    public Beacon(TileEntityBeacon block) {
        this(null, block);
    }

    public Beacon(net.minecraft.inventory.Container oContainer, TileEntityBeacon block) {
        super(oContainer, block, "Beacon");
    }

    public Beacon(net.canarymod.api.world.blocks.Beacon beacon) {
        super(beacon, "Beacon");
    }

    /**
     * Checks through the list of supported potions to see if the input effect
     * is one of them.
     *
     * @param effect
     * @return
     */
    public boolean isValidEffect(PotionEffect.Type effect) {
        return getRecodeHandle().isValidEffect(PotionEffectType.fromId(effect.getId()));
    }

    /**
     * Checks through the list of supported potions up to the specified level
     *
     * @param effect
     * @param levels
     * @return
     */
    public boolean isValidEffectAtLevels(PotionEffect.Type effect, int levels) {
        return getRecodeHandle().isValidEffectAtLevels(PotionEffectType.fromId(effect.getId()), levels);
    }

    public PotionEffect.Type getPrimaryEffect() {
        return PotionEffect.Type.fromId(getRecodeHandle().getPrimaryEffect().getID());
    }

    /**
     * Sets the potion effect *IF* the effect is on the supported potion list
     *
     * @param effect
     */
    public void setPrimaryEffect(PotionEffect.Type effect) {
        getRecodeHandle().setPrimaryEffect(PotionEffectType.fromId(effect.getId()));
    }

    /**
     * Allows mods to set other potion effects not allowed by default
     *
     * @param effect
     */
    public void setPrimaryEffectDirectly(PotionEffect.Type effect) {
        getRecodeHandle().setPrimaryEffectDirectly(PotionEffectType.fromId(effect.getId()));
    }

    public PotionEffect.Type getSecondaryEffect() {
        return PotionEffect.Type.fromId(getRecodeHandle().getSecondaryEffect().getID());
    }

    /**
     * Sets the potion effect *IF* the effect is on the supported potion list
     *
     * @param effect
     */
    public void setSecondaryEffect(PotionEffect.Type effect) {
        getRecodeHandle().setSecondaryEffect(PotionEffectType.fromId(effect.getId()));
    }

    /**
     * Allows mods to set other potion effects not allowed by default
     *
     * @param effect
     */
    public void setSecondaryEffectDirectly(PotionEffect.Type effect) {
        getRecodeHandle().setSecondaryEffectDirectly(PotionEffectType.fromId(effect.getId()));
    }

    @Override
    public net.canarymod.api.world.blocks.Beacon getRecodeHandle() {
        return (net.canarymod.api.world.blocks.Beacon) tileEntity.getCanaryBeacon();
    }
}
