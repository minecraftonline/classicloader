package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.projectile.EntitySnowball;

import static net.canarymod.api.entity.EntityType.SNOWBALL;

/**
 * Interface for the OEntitySnowball class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.entity.throwable.Snowball.class)
public class Snowball extends ThrowableProjectile {

    /**
     * Wraps an OEntitySnowball
     *
     * @param base the OEntitySnowball to wrap
     */
    public Snowball(EntitySnowball base) {
        super(base);
    }

    /**
     * Creates a new Snowball
     *
     * @param world the world to create it in
     */
    public Snowball(World world) {
        this((net.canarymod.api.entity.throwable.Snowball) factory.newThrowable(SNOWBALL, world.getRecodeHandle()));
    }

    /**
     * Creates a new Snowball
     *
     * @param world the world to create it in
     * @param shooter the shooter of the snowball
     */
    public Snowball(World world, LivingEntity shooter) {
        this((net.canarymod.api.entity.throwable.Snowball) factory.newThrowable(SNOWBALL, world.getRecodeHandle()));
    }

    /**
     * Creates a new Snowball
     *
     * @param world the world to create it in
     * @param x the x coordinate to create it at
     * @param y the y coordinate to create it at
     * @param z the z coordinate to create it at
     */
    public Snowball(World world, double x, double y, double z) {
        this(new Location(world, x, y, z));
    }

    /**
     * Creates a new Snowball
     *
     * @param loc the location at which to create it
     */
    public Snowball(Location loc) {
        this((net.canarymod.api.entity.throwable.Snowball) factory.newThrowable(SNOWBALL, loc.getRecodeLocation()));
    }

    public Snowball(net.canarymod.api.entity.throwable.Snowball snowball) {
        super(snowball);
    }

    @Override
    public EntitySnowball getEntity() {
        return (EntitySnowball) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.throwable.Snowball getRecodeHandle() {
        return (net.canarymod.api.entity.throwable.Snowball) super.getRecodeHandle();
    }
}
