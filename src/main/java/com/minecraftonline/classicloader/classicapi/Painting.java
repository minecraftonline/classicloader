package com.minecraftonline.classicloader.classicapi;

import java.util.EnumMap;
import net.canarymod.api.entity.hanging.Painting.ArtType;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.item.EntityPainting;
import net.minecraft.entity.item.EntityPainting.EnumArt;

/**
 * Interface for paintings
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.entity.hanging.Painting.class)
public class Painting extends HangingEntity {

    public enum Type {

        /**
         * A kebab with three green chili peppers
         */
        KEBAB(EnumArt.Kebab),
        /**
         * Free-look perspective of the map de_aztec from the video game Counter
         * Strike
         */
        AZTEC(EnumArt.Aztec),
        /**
         * A man wearing a fez in a desert-type land standing next to a house
         * and a bush
         */
        ALBAN(EnumArt.Alban),
        /**
         * Free-look perspective of the map de_aztec from the video game Counter
         * Strike
         */
        AZTEC2(EnumArt.Aztec2),
        /**
         * Painting of the Counter Strike map de_dust2
         */
        BOMB(EnumArt.Bomb),
        /**
         * Still life painting of two plants in pots
         */
        PLANT(EnumArt.Plant),
        /**
         * Painting of a view of some wastelands
         */
        WASTELAND(EnumArt.Wasteland),
        /**
         * Some men and women skinny-dipping in a pool over a cube of sorts
         */
        POOL(EnumArt.Pool),
        /**
         * Two hikers with pointy beards seemingly greeting each other
         */
        COURBET(EnumArt.Courbet),
        /**
         * Painting of a view of mountains and a lake, with a small photo of a
         * mountan and a plant with red dots on the window ledge
         */
        SEA(EnumArt.Sea),
        /**
         * Painting of a view of mountains at sunset
         */
        SUNSET(EnumArt.Sunset),
        /**
         * Painting of a view of mountains and a lake, with a small photo of a
         * mountain and a creeper looking at the viewer through a window
         */
        CREEBET(EnumArt.Creebet),
        /**
         * Painting of a man with a walking stick, traversing rocky plains
         */
        WANDERER(EnumArt.Wanderer),
        /**
         * A small picture of King Graham, the player character in the King's
         * Quest series
         */
        GRAHAM(EnumArt.Graham),
        /**
         * A hand holding a match, causing pixelated fire on a white cubic glass
         * fireplace
         */
        MATCH(EnumArt.Match),
        /**
         * Painting of a statue bust surrounded by pixelated fire
         */
        BUST(EnumArt.Bust),
        /**
         * Painting of scenery from Space Quest I, with the character Graham
         * from King's Quest
         */
        STAGE(EnumArt.Stage),
        /**
         * Painting of an angel praying into what appears to be a void with
         * pixelated fire below
         */
        VOID(EnumArt.Void),
        /**
         * Painting of a skeleton at night with red flowers in the foreground
         */
        SKULL_AND_ROSES(EnumArt.SkullAndRoses),
        /**
         * Painting depicting the creation of the wither
         */
        WITHER(EnumArt.Wither),
        /**
         * Two pixelated men poised to fight
         */
        FIGHTERS(EnumArt.Fighters),
        /**
         * A painting of the main character of the classic Atari game
         * International Karate fighting a large hand
         */
        POINTER(EnumArt.Pointer),
        /**
         * Painting of a girl that is pointing to a pig on a canvas
         */
        PIG_SCENE(EnumArt.Pigscene),
        /**
         * A skull on pixelated fire, in the background there is a moon in a
         * clear night sky
         */
        BURNING_SKULL(EnumArt.BurningSkull),
        /**
         * A painting of the "Mean Midget" from Grim Fandago
         */
        SKELETON(EnumArt.Skeleton),
        /**
         * A paper-looking screenshot of level 100 from the original Donkey Kong
         * arcade game
         */
        DONKEY_KONG(EnumArt.DonkeyKong);
        private static EnumMap<EnumArt, Type> map;
        private final EnumArt base;

        private Type(EnumArt base) {
            this.base = base;
            add(base, this);
        }

        /**
         * Returns the EnumArt that this wraps
         *
         * @return
         */
        public EnumArt getBase() {
            return this.base;
        }

        /**
         * Returns the width of this painting in pixels
         *
         * @return
         */
        public int getWidth() {
            // SRG return this.base.field_75703_B;
            return this.base.C;
        }

        /**
         * Returns the height of this painting in pixels
         *
         * @return
         */
        public int getHeight() {
            // SRG return this.base.field_75704_C;
            return this.base.D;
        }

        private static void add(EnumArt base, Type type) {
            if (map == null) {
                map = new EnumMap<EnumArt, Type>(EnumArt.class);
            }
            map.put(base, type);
        }

        /**
         * Get the Canary type for an <tt>EnumArt</tt>.
         *
         * @param base The <tt>EnumArt</tt> to get the Canary type for.
         * @return The Canary <tt>Type</tt> for <tt>base</tt>
         */
        public static Type fromBase(EnumArt base) {
            return map.get(base);
        }
    }

    /**
     * Creates a new painting wrapper
     *
     * @param entity The entity to wrap
     */
    public Painting(EntityPainting entity) {
        super(entity);
    }

    /**
     * Creates a new painting
     *
     * @param world The world in which to create it
     */
    public Painting(World world) {
        super(new EntityPainting(world.getWorld()));
    }

    /**
     * Creates a new painting
     *
     * @param world The world in which to create it
     * @param x The x coordinate at which to create it
     * @param y The y coordinate at which to create it
     * @param z The z coordinate at which to create it
     */
    public Painting(World world, int x, int y, int z) {
        this(world, x, y, z, Position.NORTH_FACE);
    }

    /**
     * Creates a new painting
     *
     * @param world The world in which to create it
     * @param x The x coordinate at which to create it
     * @param y The y coordinate at which to create it
     * @param z The z coordinate at which to create it
     * @param position The position in which to create it
     */
    public Painting(World world, int x, int y, int z, Position position) {
        this(new EntityPainting(world.getWorld(), x, y, z, position.ordinal()));
    }

    public Painting(net.canarymod.api.entity.hanging.Painting painting) {
        super(painting);
    }

    @Override
    public EntityPainting getEntity() {
        return (EntityPainting) super.getEntity();
    }

    /**
     * Returns the type of painting this is
     *
     * @return
     */
    public Type getType() {
        return Type.values()[getRecodeHandle().getArtType().ordinal()];
    }

    /**
     * Sets the type of painting this is
     *
     * @param type
     */
    public void setType(Type type) {
        getRecodeHandle().setArtType(ArtType.values()[type.ordinal()]);
    }

    /**
     * Returns the width of the painting in pixels
     *
     * @return
     */
    public int getWidth() {
        return getRecodeHandle().getSizeX();
    }

    /**
     * Returns the height of the painting in pixels
     *
     * @return
     */
    public int getHeight() {
        return getRecodeHandle().getSizeY();
    }

    @Override
    public net.canarymod.api.entity.hanging.Painting getRecodeHandle() {
        return (net.canarymod.api.entity.hanging.Painting) super.getRecodeHandle();
    }
}
