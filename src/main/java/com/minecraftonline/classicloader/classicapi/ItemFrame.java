package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.item.EntityItemFrame;

import static net.canarymod.api.entity.EntityType.ITEMFRAME;

/**
 * Interface for item frames
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.entity.hanging.ItemFrame.class)
public class ItemFrame extends HangingEntity {

    /**
     * Creates a new ItemFrame wrapper
     *
     * @param entity The item frame entity to wrap
     */
    public ItemFrame(EntityItemFrame entity) {
        super(entity);
    }

    /**
     * Creates a new ItemFrame
     *
     * @param world The world it create it in
     */
    public ItemFrame(World world) {
        super((net.canarymod.api.entity.hanging.ItemFrame) factory.newEntity(ITEMFRAME, world.getRecodeHandle()));
    }

    /**
     * Creates a new ItemFrame
     *
     * @param world The world to create it in
     * @param x The x coordinate at which to create it
     * @param y The y coordinate at which to create it
     * @param z The z coordinate at which to create it
     */
    public ItemFrame(World world, int x, int y, int z) {
        this(world, x, y, z, Position.NORTH_FACE);
    }

    /**
     * Creates a new ItemFrame
     *
     * @param world The world to create it in
     * @param x The x coordinate at which to create it
     * @param y The y coordinate at which to create it
     * @param z The z coordinate at which to create it
     * @param position The position in which to create it
     */
    public ItemFrame(World world, int x, int y, int z, Position position) {
        this((net.canarymod.api.entity.hanging.ItemFrame) factory.newEntity(ITEMFRAME,
                new Location(world, x, y, z).getRecodeLocation()));
    }

    public ItemFrame(net.canarymod.api.entity.hanging.ItemFrame frame) {
        super(frame);
    }

    @Override
    public EntityItemFrame getEntity() {
        return (EntityItemFrame) super.getEntity();
    }

    /**
     * Returns the item visible in the frame
     *
     * @return
     */
    public Item getFramedItem() {
        return Wrapper.wrap(getRecodeHandle().getItemInFrame());
    }

    /**
     * Sets the item displayed in the frame
     *
     * @param item The item to be displayed
     */
    public void setFramedItem(Item item) {
        getRecodeHandle().setItemInFrame(item.getRecodeHandle());
    }

    @Override
    public void setPosition(Position position) {
        if (position.ordinal() > 3) {
            throw new IllegalArgumentException("You cannot set item frames to center positions");
        }
        super.setPosition(position);
    }

    @Override
    public net.canarymod.api.entity.hanging.ItemFrame getRecodeHandle() {
        return (net.canarymod.api.entity.hanging.ItemFrame) super.getRecodeHandle();
    }
}
