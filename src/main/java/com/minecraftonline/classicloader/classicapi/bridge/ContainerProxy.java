/*
 * Copyright (C) 2013 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.classicloader.classicapi.bridge;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.minecraftonline.classicloader.classicapi.Container;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import lombok.Lombok;
import lombok.NonNull;
import net.canarymod.api.inventory.CanaryItem;
import net.canarymod.api.inventory.Inventory;
import net.canarymod.api.inventory.Item;
import net.minecraft.item.ItemStack;

/**
 *
 * @author Willem Mulder
 */
public class ContainerProxy implements Container<ItemStack> {

    private static final Cache<Inventory, ContainerProxy> cache = CacheBuilder.newBuilder().weakKeys().build();

    private final Inventory inventory;

    private ContainerProxy(@NonNull Inventory inventory) {
        this.inventory = inventory;
    }

    public static ContainerProxy of(final Inventory inventory) {
        try {
            return inventory == null ? null : cache.get(inventory, new Callable<ContainerProxy>() {
                @Override
                public ContainerProxy call() throws Exception {
                    return new ContainerProxy(inventory);
                }
            });
        } catch (ExecutionException ex) {
            throw Lombok.sneakyThrow(ex);
        }
    }

    @Override
    public ItemStack[] getContents() {
        return toNativeArray(inventory.getContents());
    }

    @Override
    public void setContents(ItemStack[] contents) {
        inventory.setContents(fromNativeArray(contents));
    }

    @Override
    public ItemStack getContentsAt(int slot) {
        return inventory.getSlot(slot) == null ? null : ((CanaryItem) inventory.getSlot(slot)).getHandle();
    }

    @Override
    public void setContentsAt(int slot, ItemStack item) {
        inventory.setSlot(slot, item == null ? null : item.getCanaryItem());
    }

    @Override
    public int getContentsSize() {
        return inventory.getSize();
    }

    @Override
    public String getName() {
        return inventory.getInventoryName();
    }

    @Override
    public void setName(String name) {
        inventory.setInventoryName(name);
    }

    private ItemStack[] toNativeArray(Item[] contents) {
        if (contents == null) {
            return null;
        }

        ItemStack[] nms = new ItemStack[contents.length];

        for (int i = 0; i < contents.length; i++) {
            nms[i] = ((CanaryItem) contents[i]).getHandle();
        }

        return nms;
    }

    private Item[] fromNativeArray(ItemStack[] nms) {
        if (nms == null) {
            return null;
        }

        Item[] contents = new Item[nms.length];

        for (int i = 0; i < nms.length; i++) {
            contents[i] = nms[i].getCanaryItem();
        }

        return contents;
    }

    public Inventory getInventory() {
        return inventory;
    }
}
