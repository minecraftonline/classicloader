/*
 * Copyright (C) 2013 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.classicloader.classicapi.bridge;

import com.minecraftonline.classicloader.classicapi.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

/**
 * {@link Container Container&lt;ItemStack&gt;}-implementation for {@link IInventory} classes.
 * @author Willem Mulder
 */
public class IInventoryContainer implements Container<ItemStack> {

    private IInventory inv;

    public IInventoryContainer(IInventory inv) {
        this.inv = inv;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ItemStack[] getContents() {
        ItemStack[] contents = new ItemStack[getContentsSize()];

        for (int i = 0; i < contents.length; i++) {
            contents[i] = getContentsAt(i);
        }

        return contents;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setContents(ItemStack[] values) {
        if (values.length != getContentsSize()) {
            throw new IllegalArgumentException("ItemStack[] length not equal to inventory size");
        }

        for (int i = 0; i < values.length; i++) {
            setContentsAt(i, values[i]);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ItemStack getContentsAt(int index) {
        return inv.a(index);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setContentsAt(int index, ItemStack value) {
        inv.a(index, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getContentsSize() {
        return inv.a();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        return inv.b();
    }

    /**
     * {@inheritDoc}
     *
     * This method isn't actually implemented, since IInventory doesn't specify a way to set a name.
     */
    @Override
    public void setName(String value) {
        // Meh.
    }
}
