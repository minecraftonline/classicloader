package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.projectile.EntityArrow;

import static net.canarymod.api.entity.EntityType.ARROW;

/**
 * Interface for the EntityArrow class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.entity.Arrow.class)
public class Arrow extends Projectile.GravityAffectedProjectile {

    /**
     * Wraps an EntityArrow
     *
     * @param base the entity to wrap
     */
    public Arrow(EntityArrow base) {
        super(base);
    }

    /**
     * Creates a new Arrow
     *
     * @param world the world to create it in
     */
    public Arrow(World world) {
        this((net.canarymod.api.entity.Arrow) factory.newEntity(ARROW, world.getRecodeHandle()));
    }

    /**
     * Creates a new Arrow
     *
     * @param location the location to create it at
     */
    public Arrow(Location location) {
        this((net.canarymod.api.entity.Arrow) factory.newEntity(ARROW, location.getRecodeLocation()));
    }

    /**
     * Creates a new Arrow
     *
     * @param world the world to create it in
     * @param x the x coordinate to create it at
     * @param y the y coordinate to create it at
     * @param z the z coordinate to create it at
     */
    public Arrow(World world, double x, double y, double z) {
        this(new Location(world, x, y, z, 0, 0));
    }

    /**
     * Creates a new Arrow
     *
     * @param world the world to create it in
     * @param shooter the shooter of the arrow
     * @param target the target of the arrow
     * @param power the greater the power, the faster the shot. 0=no forward
     * motion.
     * @param inaccuracy the lower the inaccuracy, the more accurate the shot.
     * 0=perfect.
     */
    public Arrow(World world, LivingEntity shooter, LivingEntity target, float power, float inaccuracy) {
        this((net.canarymod.api.entity.Arrow) new EntityArrow(world.getWorld(), shooter.getEntity(), target.getEntity(),
                power, inaccuracy).getCanaryEntity());
    }

    public Arrow(net.canarymod.api.entity.Arrow arrow) {
        super(arrow);
    }

    /**
     * Set the shooter of this arrow
     *
     * @param shooter the entity that shot
     */
    public void setShooter(BaseEntity shooter) {
        // SRG getEntity().field_70250_c = shooter.getEntity();
        getEntity().c = shooter.getEntity();
    }

    @Override
    public boolean setShooter(LivingEntity shooter) {
        setShooter((BaseEntity) shooter);
        return true;
    }

    @Override
    public BaseEntity getShooter() {
        return Wrapper.wrap(getRecodeHandle().getOwner());
    }

    /**
     * Returns whether or not players can pick up this arrow
     *
     * @return
     */
    public boolean canPickup() {
        return getRecodeHandle().canPickUp();
    }

    /**
     * Sets whether or not players can pick up this arrow
     *
     * @param canPickup
     */
    public void setCanPickup(boolean canPickup) {
        getRecodeHandle().setCanPickUp(canPickup);
    }

    @Override
    public EntityArrow getEntity() {
        return (EntityArrow) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.Arrow getRecodeHandle() {
        return (net.canarymod.api.entity.Arrow) super.getRecodeHandle();
    }
}
