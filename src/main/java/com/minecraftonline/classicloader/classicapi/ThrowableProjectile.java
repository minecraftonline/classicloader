package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.entity.living.LivingBase;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.projectile.EntityThrowable;

@RecodeProxy(net.canarymod.api.entity.throwable.EntityThrowable.class)
public class ThrowableProjectile extends Projectile.GravityAffectedProjectile {

    public ThrowableProjectile(EntityThrowable base) {
        super(base);
    }

    public ThrowableProjectile(net.canarymod.api.entity.throwable.EntityThrowable throwable) {
        super(throwable);
    }

    @Override
    public LivingEntityBase getShooter() {
        return Wrapper.wrap((LivingBase) getRecodeHandle().getThrower());
    }

    @Override
    public EntityThrowable getEntity() {
        return (EntityThrowable) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.throwable.EntityThrowable getRecodeHandle() {
        return (net.canarymod.api.entity.throwable.EntityThrowable) super.getRecodeHandle();
    }
}
