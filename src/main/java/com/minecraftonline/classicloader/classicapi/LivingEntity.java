package com.minecraftonline.classicloader.classicapi;

import ca.grimoire.critical.Todo;
import net.canarymod.api.entity.living.EntityLiving;
import net.canarymod.api.nbt.CompoundTag;
import com.minecraftonline.classicloader.Util;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.EntityLivingBase;

/**
 * Interface for living entities.
 */
@RecodeProxy(EntityLiving.class)
public class LivingEntity extends LivingEntityBase {

    /**
     * Interface for living entities
     */
    public LivingEntity() {
    }

    /**
     * Interface for living entities
     *
     * @param livingEntity
     */
    public LivingEntity(net.minecraft.entity.EntityLiving livingEntity) {
        super(livingEntity);
    }

    public LivingEntity(EntityLiving ent) {
        super(ent);
    }

    /**
     * Returns the entity we're wrapping.
     *
     * @return
     */
    @Override
    public net.minecraft.entity.EntityLiving getEntity() {
        return (net.minecraft.entity.EntityLiving) super.getEntity();
    }

    /**
     * Drops this mob's loot. Automatically called if health is set to 0.
     */
    public void dropLoot() {
        // SRG Util.callMethod(EntityLivingBase.class, getEntity(), "func_82160_b", new Class<?>[] {boolean.class, int.class}, new Object[] {true, 0}, void.class);
        Util.callMethod(EntityLivingBase.class, getEntity(), "a", new Class<?>[] {boolean.class, int.class}, new Object[] {true, 0}, void.class);
    }

    /**
     * Sets the item held by the entity.
     *
     * @param item
     */
    public void setItemInHand(Item item) {
        getRecodeHandle().setEquipment(item.getRecodeHandle(), 0);
    }

    /**
     * Gets the item held by the entity.
     *
     * @return
     */
    public Item getItemStackInHand() {
        net.canarymod.api.inventory.Item stack = getRecodeHandle().getItemInHand();
        return stack == null ? null : Wrapper.wrap(stack);
    }

    /**
     * Sets an armor slot of the entity.
     *
     * @param slot The slot of the armor, 0 being boots and 3 being helmet
     * @param armor The item of armor to add
     */
    public void setArmorSlot(int slot, Item armor) {
        if (slot >= 0 && slot <= 3) {
            getRecodeHandle().setEquipment(armor.getRecodeHandle(), slot + 1);
        }
    }

    /**
     * Gets the item in one of the entity's armor slots.
     *
     * @param slot The slot of the armor, 0 being boots and 3 being helmet
     * @return
     */
    public Item getArmorSlot(int slot) {
        if (slot < 0 || slot > 3) {
            return null;
        }
        net.canarymod.api.inventory.Item stack = getRecodeHandle().getEquipmentInSlot(slot + 1);
        return stack == null ? null : Wrapper.wrap(stack);
    }

    /**
     * Returns whether or not this entity will despawn naturally.
     *
     * @return
     */
    public boolean isPersistent() {
        return getRecodeHandle().isPersistenceRequired();
    }

    /**
     * Sets whether or not this entity will despawn naturally.
     *
     * @param isPersistent
     */
    @Todo("Implement in recode methods when possible")
    public void setPersistent(boolean isPersistent) {
        CompoundTag props = getRecodeHandle().getNBT();
        props.put("PersistenceRequired", isPersistent);
        getRecodeHandle().setNBT(props);
    }

    /**
     * Returns the drop chance of an item owned by this entity.
     *
     * @param slot The slot of the item, 0-4: 0 is hand, 1-4 are armor slots
     * (1=boots and 4=helmet)
     * @return The drop chance, 0-1 (anything greater than 1 is guaranteed to
     * drop)
     */
    public float getDropChance(int slot) {
        return getRecodeHandle().getDropChance(slot);
    }

    /**
     * Sets the drop chance of an item owned by this entity.
     *
     * @param slot The slot of the item, 0-4: 0 is hand, 1-4 are armor slots
     * (1=boots and 4=helmet)
     * @param chance The drop chance, 0-1 (anything greater than 1 is guaranteed
     * to drop)
     */
    public void setDropChance(int slot, float chance) {
        getRecodeHandle().setDropChance(slot, chance);
    }

    /**
     * Whether or not this entity can pick up items.
     *
     * @return
     */
    public boolean canPickUpLoot() {
        return getRecodeHandle().canPickUpLoot();
    }

    /**
     * Sets whether or not this entity can pick up items.
     *
     * @param flag
     */
    public void setCanPickUpLoot(boolean flag) {
        getRecodeHandle().setCanPickUpLoot(flag);
    }

    /**
     * Returns this entity's custom name.
     *
     * @return This entity's custom name if it has one, an empty string
     * otherwise
     */
    public String getCustomName() {
        return getRecodeHandle().getDisplayName();
    }

    /**
     * Sets this entity's custom name.
     *
     * @param name This entity's new custom name
     */
    public void setCustomName(String name) {
        getRecodeHandle().setDisplayName(name);
    }

    /**
     * Returns whether this entity has a custom name.
     *
     * @return <tt>true</tt> if this entity has a custom name, <tt>false</tt>
     * otherwise.
     */
    public boolean hasCustomName() {
        return getRecodeHandle().hasDisplayName();
    }

    /**
     * Returns whether a possible custom name is shown on this entity.
     *
     * @return <tt>true</tt> if a possible custom name is shown, <tt>false</tt>
     * otherwise.
     */
    public boolean isCustomNameVisible() {
        return getRecodeHandle().showingDisplayName();
    }

    /**
     * Sets whether a possible custom name is shown on this entity.
     *
     * @param visible <tt>true</tt> to show a possible custom name,
     * <tt>false</tt> to hide it.
     */
    public void setCustomNameVisible(boolean visible) {
        getRecodeHandle().setShowDisplayName(visible);
    }

    @Override
    public EntityLiving getRecodeHandle() {
        return (EntityLiving) super.getRecodeHandle();
    }
}
