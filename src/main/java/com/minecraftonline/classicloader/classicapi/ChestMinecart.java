package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.entity.EntityType;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.item.EntityMinecartChest;

@RecodeProxy(net.canarymod.api.entity.vehicle.ChestMinecart.class)
public class ChestMinecart extends ContainerMinecart {

    ChestMinecart(EntityMinecartChest o) {
        super(o);
    }

    public ChestMinecart(net.canarymod.api.entity.vehicle.ChestMinecart cart) {
        super(cart);
    }

    /**
     * Create a new Storage Minecart with the given position. Call
     * {@link #spawn()} to spawn it in the world.
     *
     * @param world The world for the new minecart
     * @param x The x coordinate for the new minecart
     * @param y The y coordinate for the new minecart
     * @param z The z coordinate for the new minecart
     */
    public ChestMinecart(World world, double x, double y, double z) {
        this((net.canarymod.api.entity.vehicle.ChestMinecart) Canary.factory().getEntityFactory()
                .newVehicle(EntityType.CHESTMINECART,
                            new net.canarymod.api.world.position.Location(world.getRecodeHandle(), x, y, z, 0, 0)));
    }

    @Override
    public EntityMinecartChest getEntity() {
        return (EntityMinecartChest) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.vehicle.ChestMinecart getRecodeHandle() {
        return (net.canarymod.api.entity.vehicle.ChestMinecart) super.getRecodeHandle();
    }
}
