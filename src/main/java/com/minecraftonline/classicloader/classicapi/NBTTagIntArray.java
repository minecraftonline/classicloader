package com.minecraftonline.classicloader.classicapi;

import java.util.Arrays;
import net.canarymod.Canary;
import net.canarymod.api.nbt.IntArrayTag;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for the net.minecraft.server.NBTTagIntArray class
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(IntArrayTag.class)
public class NBTTagIntArray extends NBTBase {

    /**
     * Wraps an net.minecraft.server.NBTTagIntArray
     *
     * @param baseTag the tag to wrap
     */
    public NBTTagIntArray(net.minecraft.nbt.NBTTagIntArray baseTag) {
        super(baseTag);
    }

    /**
     * Creates a new NBTTagIntArray
     *
     * @param name the name of the tag
     */
    public NBTTagIntArray(String name) {
        this(name, null);
    }

    /**
     * Creates a new NBTTagIntArray
     *
     * @param name the name of the tag
     * @param value the value of the tag
     */
    public NBTTagIntArray(String name, int[] value) {
        this(Canary.factory().getNBTFactory().newIntArrayTag(value));
    }

    public NBTTagIntArray(IntArrayTag intArrayTag) {
        super(intArrayTag);
    }

    @Override
    public net.minecraft.nbt.NBTTagIntArray getBaseTag() {
        return (net.minecraft.nbt.NBTTagIntArray) super.getBaseTag();
    }

    /**
     * Returns the value of this tag
     *
     * @return
     */
    public int[] getValue() {
        return getRecodeHandle().getValue();
    }

    /**
     * Sets the value of this tag
     *
     * @param value the new value
     */
    public void setValue(int[] value) {
        getRecodeHandle().setValue(value);
    }

    @Override
    public String toString() {
        return String.format("NBTTag[type=%s, name=%s, value=%s]", getTagName(getType()), getName(), Arrays.toString(getValue()));
    }

    @Override
    public IntArrayTag getRecodeHandle() {
        return (IntArrayTag) super.getRecodeHandle();
    }

    public static NBTTagIntArray[] fromRecodeArray(IntArrayTag[] recode) {
        NBTTagIntArray[] classic = new NBTTagIntArray[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new NBTTagIntArray(recode[i]);
        }
        return classic;
    }

    public static IntArrayTag[] toRecodeArray(NBTTagIntArray[] classic) {
        IntArrayTag[] recode = new IntArrayTag[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
