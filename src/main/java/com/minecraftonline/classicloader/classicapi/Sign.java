package com.minecraftonline.classicloader.classicapi;

import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.tileentity.TileEntitySign;

/**
 * Sign.java - Interface to tileEntitys
 *
 * @author James
 */
@RecodeProxy(net.canarymod.api.world.blocks.Sign.class)
public class Sign extends ComplexBlockBase<TileEntitySign> {

    /**
     * Creates a tileEntity interface
     *
     * @param localSign
     */
    public Sign(TileEntitySign localSign) {
        super(localSign);
    }

    public Sign(net.canarymod.api.world.blocks.Sign sign) {
        super(sign);
    }

    /**
     * Sets the line of text at specified index
     *
     * @param index line
     * @param text text
     */
    public void setText(int index, String text) {
        getRecodeHandle().setTextOnLine(text, index);
    }

    /**
     * Returns the line of text
     *
     * @param index line of text
     * @return text
     */
    public String getText(int index) {
        return getRecodeHandle().getTextOnLine(index);
    }

    /**
     * Returns a String value representing this Block
     *
     * @return String representation of this block
     */
    @Override
    public String toString() {
        return String.format("Sign [x=%d, y=%d, z=%d]", getX(), getY(), getZ());
    }

    /**
     * Tests the given object to see if it equals this object
     *
     * @param obj the object to test
     * @return true if the two objects match
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sign other = (Sign) obj;

        if (getX() != other.getX()) {
            return false;
        }
        if (getY() != other.getY()) {
            return false;
        }
        if (getZ() != other.getZ()) {
            return false;
        }
        if (!getWorld().equals(other.getWorld())) {
            return false;
        }
        return true;
    }

    /**
     * Returns a semi-unique hashcode for this block
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        int hash = 7;

        hash = 97 * hash + getX();
        hash = 97 * hash + getY();
        hash = 97 * hash + getZ();
        hash = 97 * hash + getWorld().hashCode();
        return hash;
    }

    @Override
    public net.canarymod.api.world.blocks.Sign getRecodeHandle() {
        return (net.canarymod.api.world.blocks.Sign) super.getRecodeHandle();
    }
}
