package com.minecraftonline.classicloader.classicapi;

import ca.grimoire.critical.Todo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Lombok;
import net.canarymod.api.entity.Entity;
import net.canarymod.api.entity.EntityItem;
import net.canarymod.api.entity.living.EntityLiving;
import net.canarymod.api.entity.living.animal.EntityAnimal;
import net.canarymod.api.entity.living.monster.EntityMob;
import net.canarymod.api.entity.vehicle.Vehicle;
import net.canarymod.api.world.CanaryWorld;
import net.canarymod.api.world.blocks.Jukebox;
import net.canarymod.api.world.blocks.TileEntity;
import net.canarymod.api.world.effects.SoundEffect;
import net.canarymod.api.world.position.Position;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.world.WorldServer;

/**
 * World.java - Interface to worlds. Most of the stuff in Server.java was moved
 * here.
 *
 * @author 14mRh4X0r
 * @author Chris
 */
@RecodeProxy(net.canarymod.api.world.World.class)
public class World {

    private final net.canarymod.api.world.World world;

    public enum Dimension {

        /**
         * Represents the Nether.
         */
        NETHER(-1), //
        /**
         * Represents the overworld.
         */
        NORMAL(0), //
        /**
         * Represents the End.
         */
        END(1);
        private int id;
        private static Map<Integer, Dimension> map;

        @SuppressWarnings("LeakingThisInConstructor")
        private Dimension(int id) {
            this.id = id;
            add(id, this);
        }

        private static void add(int type, Dimension name) {
            if (map == null) {
                map = new HashMap<Integer, Dimension>();
            }

            map.put(type, name);
        }

        /**
         * Returns this world's ID.
         *
         * @return this world's ID.
         */
        public int getId() {
            return id;
        }

        /**
         * Get a <tt>Dimension</tt> by ID.
         *
         * @param type The ID for the <tt>Dimension</tt> to return
         * @return The <tt>Dimension</tt> for the given ID.
         * @see #getId()
         */
        public static Dimension fromId(final int type) {
            return map.get(type);
        }

        /**
         * Returns the array index for use with, e.g.,
         * {@link Server#getWorld(java.lang.String)}
         *
         * @return The array index for this <tt>Dimension</tt>
         */
        public int toIndex() {
            return id == 0 ? 0 : id == -1 ? 1 : 2;
        }

        @Override
        public String toString() {
            String name = this.name().toLowerCase();
            return name.substring(0, 1).toUpperCase() + name.substring(1);
        }
    }

    public enum Type {

        // SRG DEFAULT(net.minecraft.world.WorldType.field_77137_b),
        DEFAULT(net.minecraft.world.WorldType.b),
        // SRG FLAT(net.minecraft.world.WorldType.field_77138_c),
        FLAT(net.minecraft.world.WorldType.c),
        // SRG LARGE_BIOMES(net.minecraft.world.WorldType.field_77135_d),
        LARGE_BIOMES(net.minecraft.world.WorldType.d),
        // SRG DEFAULT_1_1(net.minecraft.world.WorldType.field_77136_e);
        DEFAULT_1_1(net.minecraft.world.WorldType.f);
        private net.minecraft.world.WorldType nativeType;

        private Type(net.minecraft.world.WorldType nativeType) {
            this.nativeType = nativeType;
        }

        public net.minecraft.world.WorldType getNative() {
            return nativeType;
        }
    }

    /**
     * Instantiates this wrapper around {@code world}.
     *
     * @param world the WorldServer to wrap
     */
    public World(WorldServer world) {
        this(world.getCanaryWorld());
    }

    public World(net.canarymod.api.world.World world) {
        this.world = world;
    }

    /**
     * Returns the WorldServer this class wraps around.
     *
     * @return the managed worldserver
     */
    public WorldServer getWorld() {
        return (WorldServer) ((CanaryWorld) world).getHandle();
    }

    /**
     * Returns this dimension's type. Currently Nether, End and Normal.
     *
     * @return the dimension type
     */
    public Dimension getType() {
        return Dimension.fromId(world.getType().getId());
    }

    /**
     * Returns actual server time (-2^63 to 2^63-1).
     *
     * @return time server time
     */
    public long getTime() {
        return world.getTotalTime();
    }

    /**
     * Returns current server time (0-24000).
     *
     * @return time server time
     */
    public long getRelativeTime() {
        return world.getRelativeTime();
    }

    /**
     * Sets the actual server time.
     *
     * @param time time (-2^63 to 2^63-1)
     */
    public void setTime(long time) {
        // SRG getWorld().field_72986_A.func_82572_b(time); // Not available in Lib/Recode yet
        getWorld().x.b(time); // Not available in Lib/Recode yet
    }

    /**
     * Sets the current server time.
     *
     * @param time time (0-24000)
     */
    public void setRelativeTime(long time) {
        world.setTime(time);
    }

    /**
     * Returns the list of mobs in all open chunks.
     *
     * @return list of mobs
     */
    public List<Mob> getMobList() {
        try {
            return Wrapper.wrapList(world.getMobList(), EntityMob.class);
        } catch (NoSuchMethodException ignored) {
            throw Lombok.sneakyThrow(ignored);
        }
    }

    /**
     * Returns the list of animals in all open chunks.
     *
     * @return list of animals
     */
    public List<Mob> getAnimalList() {
        try {
            return Wrapper.wrapList(world.getAnimalList(), EntityAnimal.class);
        } catch (NoSuchMethodException ignored) {
            throw Lombok.sneakyThrow(ignored);
        }
    }

    /**
     * Returns the list of minecarts in all open chunks.
     *
     * @return list of minecarts
     */
    public List<Minecart> getMinecartList() {
        try {
            return Wrapper.wrapList(world.getMinecartList(), net.canarymod.api.entity.vehicle.Minecart.class);
        } catch (NoSuchMethodException ignored) {
            throw Lombok.sneakyThrow(ignored);
        }
    }

    /**
     * Returns the list of boats in all open chunks.
     *
     * @return list of boats
     */
    public List<Boat> getBoatList() {
        try {
            return Wrapper.wrapList(world.getBoatList(), net.canarymod.api.entity.vehicle.Boat.class);
        } catch (NoSuchMethodException ignored) {
            throw Lombok.sneakyThrow(ignored);
        }
    }

    /**
     * Returns the list of all entities in the server in open chunks.
     *
     * @return list of entities
     */
    public List<BaseEntity> getEntityList() {
        try {
            return Wrapper.wrapList(world.getTrackedEntities(), Entity.class);
        } catch (NoSuchMethodException ignored) {
            throw Lombok.sneakyThrow(ignored);
        }
    }

    /**
     * Returns the list of items in all open chunks.
     *
     * @return list of items
     */
    public List<ItemEntity> getItemList() {
        try {
            return Wrapper.wrapList(world.getItemList(), EntityItem.class);
        } catch (NoSuchMethodException ignored) {
            throw Lombok.sneakyThrow(ignored);
        }
    }

    /**
     * Returns the list of all living entities (players, animals, mobs) in open
     * chunks.
     *
     * @return list of living entities
     */
    public List<LivingEntity> getLivingEntityList() {
        try {
            return Wrapper.wrapList(world.getEntityLivingList(), EntityLiving.class);
        } catch (NoSuchMethodException ignored) {
            throw Lombok.sneakyThrow(ignored);
        }
    }

    /**
     * Returns the list of vehicles in open chunks.
     *
     * @return list of vehicles
     */
    public List<BaseVehicle> getVehicleEntityList() {
        try {
            return Wrapper.wrapList(world.getVehicleList(), Vehicle.class);
        } catch (NoSuchMethodException ignored) {
            throw Lombok.sneakyThrow(ignored);
        }
    }

    /**
     * Get the global spawn location
     *
     * @return Location object for spawn
     */
    public Location getSpawnLocation() {
        return new Location(world.getSpawnLocation());
    }

    /**
     * Sets the spawn location for this level, NOT only this world.
     *
     * @param x The spawn's new x location
     * @param y The spawn's new y location
     * @param z The spawn's new z location
     */
    public void setSpawnLocation(int x, int y, int z) {
        world.setSpawnLocation(new net.canarymod.api.world.position.Location(x, y, z));
    }

    /**
     * Sets the spawn location for this level, NOT only this world.
     *
     * @param location The new spawn location.
     */
    public void setSpawnLocation(Location location) {
        this.setSpawnLocation(etc.floor(location.x), etc.floor(location.y), etc.floor(location.z));
    }

    /**
     * Sets the block
     *
     * @param block
     * @return
     */
    public boolean setBlock(Block block) {
        return setBlockAt(block.getType(), block.getX(), block.getY(), block.getZ()) && setBlockData(block.getX(), block.getY(), block.getZ(), block.getData());
    }

    /**
     * Returns the block at the specified location
     *
     * @param x
     * @param y
     * @param z
     * @return block
     */
    public Block getBlockAt(int x, int y, int z) {
        return new Block(this, getBlockIdAt(x, y, z), x, y, z, getBlockData(x, y, z));
    }

    /**
     * Returns the block data at the specified coordinates
     *
     * @param x x
     * @param y y
     * @param z z
     * @return block data
     */
    public int getBlockData(int x, int y, int z) {
        return world.getDataAt(x, y, z);
    }

    /**
     * Sets the block data at the specified coordinates
     *
     * @param x x
     * @param y y
     * @param z z
     * @param data data
     * @return true if it was successful
     */
    public boolean setBlockData(int x, int y, int z, int data) {
        world.setDataAt(x, y, z, (short) data);
        return true;
    }

    /**
     * Sets the block type at the specified location
     *
     * @param blockType
     * @param x
     * @param y
     * @param z
     * @return true if successful
     */
    public boolean setBlockAt(int blockType, int x, int y, int z) {
        world.setBlockAt(x, y, z, (short) blockType);
        return true;
    }

    /**
     * Returns the highest block Y
     *
     * @param x
     * @param z
     * @return highest block altitude
     */
    public int getHighestBlockY(int x, int z) {
        return world.getHighestBlockAt(x, z);
    }

    /**
     * Returns the block type at the specified location
     *
     * @param x
     * @param y
     * @param z
     * @return block type
     */
    public int getBlockIdAt(int x, int y, int z) {
        return world.getBlockAt(x, y, z).getTypeId();
    }

    /**
     * Returns the complex block at the specified location. Null if there's no
     * complex block there. This will also find complex-blocks spanning multiple
     * spaces, such as double chests.
     *
     * @param block
     * @return complex block
     */
    public ComplexBlock getComplexBlock(Block block) {
        return getComplexBlock(block.getX(), block.getY(), block.getZ());
    }

    /**
     * Returns the complex block at the specified location. Null if there's no
     * complex block there. This will also find complex-blocks spanning multiple
     * spaces, such as double chest.
     *
     * @param location The location of the block. Only x/y/z is used.
     * @return ComplexBlock
     */
    public ComplexBlock getComplexBlock(Location location) {
        return getComplexBlock((int) location.x, (int) location.y, (int) location.z);
    }

    /**
     * Returns the complex block at the specified location. Null if there's no
     * complex block there. This will also find complex-blocks spanning multiple
     * spaces, such as double chests.
     *
     * @param x x
     * @param y y
     * @param z z
     * @return complex block
     */
    public ComplexBlock getComplexBlock(int x, int y, int z) {
        ComplexBlock result = getOnlyComplexBlock(x, y, z);

        if (result != null) {
            if (result instanceof Chest) {
                Chest chest = (Chest) result;

                result = chest.findAttachedChest();

                if (result != null) {
                    return result;
                } else {
                    return chest;
                }
            }
        }

        return result;
    }

    /**
     * Returns the only complex block at the specified location. Null if there's
     * no complex block there.
     *
     * @param block
     * @return complex block
     */
    public ComplexBlock getOnlyComplexBlock(Block block) {
        return getOnlyComplexBlock(block.getX(), block.getY(), block.getZ());
    }

    /**
     * Returns the complex block at the specified location. Null if there's no
     * complex block there.
     *
     * @param x x
     * @param y y
     * @param z z
     * @return complex block
     */
    public ComplexBlock getOnlyComplexBlock(int x, int y, int z) {
        TileEntity tileEntity = world.getOnlyTileEntityAt(x, y, z);

        if (tileEntity != null) {
            if (tileEntity instanceof net.canarymod.api.world.blocks.Beacon) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.Beacon) tileEntity);
            } else if (tileEntity instanceof net.canarymod.api.world.blocks.BrewingStand) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.BrewingStand) tileEntity);
            } else if (tileEntity instanceof net.canarymod.api.world.blocks.Chest) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.Chest) tileEntity);
            } else if (tileEntity instanceof net.canarymod.api.world.blocks.CommandBlock) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.CommandBlock) tileEntity);
            } else if (tileEntity instanceof net.canarymod.api.world.blocks.Dispenser) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.Dispenser) tileEntity);
            } else if (tileEntity instanceof net.canarymod.api.world.blocks.Furnace) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.Furnace) tileEntity);
            } else if (tileEntity instanceof net.canarymod.api.world.blocks.HopperBlock) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.HopperBlock) tileEntity);
            } else if (tileEntity instanceof Jukebox) {
                return Wrapper.wrap((Jukebox) tileEntity);
            } else if (tileEntity instanceof net.canarymod.api.world.blocks.MobSpawner) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.MobSpawner) tileEntity);
            } else if (tileEntity instanceof net.canarymod.api.world.blocks.NoteBlock) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.NoteBlock) tileEntity);
            } else if (tileEntity instanceof net.canarymod.api.world.blocks.Sign) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.Sign) tileEntity);
            } else if (tileEntity instanceof net.canarymod.api.world.blocks.Skull) {
                return Wrapper.wrap((net.canarymod.api.world.blocks.Skull) tileEntity);
            }
        }

        return null;
    }

    /**
     * Drops an item at the specified location
     *
     * @param loc
     * @param itemId
     *
     * @return returns the ItemEntity that was dropped
     */
    public ItemEntity dropItem(Location loc, int itemId) {
        return dropItem(loc.x, loc.y, loc.z, itemId, 1, 0);
    }

    /**
     * Drops an item at the specified location
     *
     * @param x
     * @param y
     * @param z
     * @param itemId
     *
     * @return returns the ItemEntity that was dropped
     */
    public ItemEntity dropItem(double x, double y, double z, int itemId) {
        return dropItem(x, y, z, itemId, 1, 0);
    }

    /**
     * Drops an item with desired quantity at the specified location
     *
     * @param x
     * @param y
     * @param z
     * @param itemId
     * @param quantity
     *
     * @return returns the ItemEntity that was dropped
     */
    public ItemEntity dropItem(double x, double y, double z, int itemId, int quantity) {
        return dropItem(x, y, z, itemId, quantity, 0);
    }

    /**
     * Drops an item with desired quantity at the specified location
     *
     * @param loc
     * @param itemId
     * @param quantity
     *
     * @return returns the ItemEntity that was dropped
     */
    public ItemEntity dropItem(Location loc, int itemId, int quantity) {
        return dropItem(loc.x, loc.y, loc.z, itemId, quantity, 0);
    }

    /**
     * Drops an item with damage data and desired quantity at the specified
     * location
     *
     * @param loc
     * @param itemId
     * @param quantity
     * @param damage
     *
     * @return returns the ItemEntity that was dropped
     */
    public ItemEntity dropItem(Location loc, int itemId, int quantity, int damage) {
        return dropItem(loc.x, loc.y, loc.z, itemId, quantity, damage);
    }

    /**
     * Drops an item with desired quantity and damage value at the specified
     * location
     *
     * @param x
     * @param y
     * @param z
     * @param itemId
     * @param quantity
     * @param damage
     * @return returns the ItemEntity that was dropped
     */
    public ItemEntity dropItem(double x, double y, double z, int itemId, int quantity, int damage) {
        return dropItem(x, y, z, new Item(itemId, quantity, -1, damage));
    }

    /**
     * Drops an item with desired quantity at the specified location
     *
     * @param loc
     * @param item
     *
     * @return returns the ItemEntity that was dropped
     */
    public ItemEntity dropItem(Location loc, Item item) {
        return dropItem(loc.x, loc.y, loc.z, item);
    }

    /**
     * Drops an item with desired quantity and damage value at the specified
     * location
     *
     * @param x
     * @param y
     * @param z
     * @param item
     * @return returns the ItemEntity that was dropped
     */
    public ItemEntity dropItem(double x, double y, double z, Item item) {
        return Wrapper.wrap(world.dropItem(new Position(x, y, z), item.getRecodeHandle()));
    }

    /**
     * Forces the server to update the physics for blocks around the given block
     *
     * @param block the block that changed
     */
    public void updateBlockPhysics(Block block) {
        updateBlockPhysics(block.getX(), block.getY(), block.getZ(), block.getData());
    }

    /**
     * Forces the server to update the physics for blocks around the given block
     *
     * @param x the X coordinate of the block
     * @param y the Y coordinate of the block
     * @param z the Z coordinate of the block
     * @param data the new data for the block
     */
    public void updateBlockPhysics(int x, int y, int z, int data) {
        this.setBlockData(x, y, z, data);
    }

    /**
     * Checks to see whether or not the chunk containing the given block is
     * loaded into memory.
     *
     * @param block the Block to check
     * @return true if the chunk is loaded
     */
    public boolean isChunkLoaded(Block block) {
        return isChunkLoaded(block.getX(), block.getY(), block.getZ());
    }

    /**
     * Checks to see whether or not the chunk containing the given block
     * coordinates is loaded into memory.
     *
     * @param x a block x-coordinate
     * @param y a block y-coordinate
     * @param z a block z-coordinate
     * @return true if the chunk is loaded
     */
    public boolean isChunkLoaded(int x, int y, int z) {
        return isChunkLoaded(x >> 4, z >> 4);
    }

    /**
     * Checks to see whether or not the chunk containing the given chunk
     * coordinates is loaded into memory.
     *
     * @param x a chunk x-coordinate
     * @param z a chunk z-coordinate
     * @return true if the chunk is loaded
     */
    public boolean isChunkLoaded(int x, int z) {
        return world.isChunkLoaded(x, z);
    }

    /**
     * Loads the chunk containing the given block. If the chunk does not exist,
     * it will be generated.
     *
     * @param block the Block to check
     * @return chunk
     */
    public Chunk loadChunk(Block block) {
        return loadChunk(block.getX(), block.getY(), block.getZ());
    }

    /**
     * Loads the chunk containing the given block coordinates. If the chunk does
     * not exist, it will be generated.
     *
     * @param x a block x-coordinate
     * @param y a block y-coordinate
     * @param z a block z-coordinate
     * @return chunk
     */
    public Chunk loadChunk(int x, int y, int z) {
        return loadChunk(x >> 4, z >> 4);
    }

    /**
     * Loads the chunk containing the given chunk coordinates. If the chunk does
     * not exist, it will be generated.
     *
     * @param x a chunk x-coordinate
     * @param z a chunk z-coordinate
     * @return chunk
     */
    public Chunk loadChunk(int x, int z) {
        return Wrapper.wrap(world.loadChunk(x, z));
    }

    /**
     * Gets the chunk containing the given block. If the chunk is not loaded,
     * the result will be null.
     *
     * @param block the Block to check
     * @return chunk
     */
    public Chunk getChunk(Block block) {
        return getChunk(block.getX(), block.getY(), block.getZ());
    }

    /**
     * Gets the chunk containing the given block coordinates. If the chunk is
     * not loaded, the result will be null.
     *
     * @param x a block x-coordinate
     * @param y a block y-coordinate
     * @param z a block z-coordinate
     * @return chunk
     */
    public Chunk getChunk(int x, int y, int z) {
        return getChunk(x >> 4, z >> 4);
    }

    /**
     * Gets the chunk containing the given chunk coordinates. If the chunk is
     * not loaded, the result will be null.
     *
     * @param x a chunk x-coordinate
     * @param z a chunk z-coordinate
     * @return chunk
     */
    public Chunk getChunk(int x, int z) {
        if (isChunkLoaded(x, z)) {
            return loadChunk(x, z);
        } else {
            return null;
        }
    }

    /**
     * Checks if the provided block is being powered through redstone
     *
     * @param block Block to check
     * @return true if the block is being powered
     */
    public boolean isBlockPowered(Block block) {
        return isBlockPowered(block.getX(), block.getY(), block.getZ());
    }

    /**
     * Checks if the provided block is being powered through redstone
     *
     * @param x a block x-coordinate
     * @param y a block y-coordinate
     * @param z a block z-coordinate
     * @return true if the block is being powered
     */
    public boolean isBlockPowered(int x, int y, int z) {
        return world.isBlockPowered(x, y, z);
    }

    /**
     * Checks if the provided block is being indirectly powered through redstone
     *
     * @param block Block to check
     * @return true if the block is being indirectly powered
     */
    public boolean isBlockIndirectlyPowered(Block block) {
        return isBlockIndirectlyPowered(block.getX(), block.getY(), block.getZ());
    }

    /**
     * Checks if the provided block is being indirectly powered through redstone
     *
     * @param x a block x-coordinate
     * @param y a block y-coordinate
     * @param z a block z-coordinate
     * @return true if the block is being indirectly powered
     */
    public boolean isBlockIndirectlyPowered(int x, int y, int z) {
        return world.isBlockIndirectlyPowered(x, y, z);
    }

    /**
     * Set the thunder state
     *
     * @param thundering whether it should thunder
     */
    @Todo("Remove hook call when recode adds it")
    public void setThundering(boolean thundering) {
        if ((Boolean) etc.getLoader().callHook(PluginLoader.Hook.THUNDER_CHANGE, this, thundering)) {
            return;
        }

        world.setThundering(thundering);
    }

    /**
     * Set the thunder ticks.
     *
     * @param ticks ticks of thunder
     */
    public void setThunderTime(int ticks) {
        world.setThunderTime(ticks);
    }

    /**
     * Sets the rain state.
     *
     * @param raining whether it should rain
     */
    @Todo("Remove hook call when recode adds it")
    public void setRaining(boolean raining) {
        if ((Boolean) etc.getLoader().callHook(PluginLoader.Hook.WEATHER_CHANGE, this, raining)) {
            return;
        }

        world.setRaining(raining);
    }

    /**
     * Sets the rain ticks.
     *
     * @param ticks ticks of rain
     */
    public void setRainTime(int ticks) {
        world.setRainTime(ticks);
    }

    /**
     * Returns whether it's thundering
     *
     * @return whether it's thundering
     */
    public boolean isThundering() {
        return world.isThundering();
    }

    /**
     * Returns the number of ticks to go till the end of the thunder
     *
     * @return the thunder ticks
     */
    public int getThunderTime() {
        return world.getThunderTicks();
    }

    /**
     * Returns whether it's raining
     *
     * @return whether it's raining
     */
    public boolean isRaining() {
        return world.isRaining();
    }

    /**
     * Returns the number of ticks to go till the end of the rain
     *
     * @return the rain ticks
     */
    public int getRainTime() {
        return world.getRainTicks();
    }

    @Override
    public boolean equals(Object obj) {
        return obj == this || obj != null && obj instanceof World && world.equals(((World) obj).world);
    }

    @Override
    public int hashCode() {
        int hash = 7;

        hash = 89 * hash + (this.world != null ? this.world.hashCode() : 0);
        return hash;
    }

    /**
     * Creates an explosion with the specified power at the given location.
     *
     * @param exploder The entity causing the explosion.
     * @param x
     * @param y
     * @param z
     * @param power The power of the explosion. TNT has a power of 4.
     */
    public void explode(BaseEntity exploder, double x, double y, double z, float power) {
        explode(exploder, x, y, z, power, true);
    }

    /**
     * Creates an explosion with the specified power at the given location.
     *
     * @param exploder The entity causing the explosion.
     * @param x
     * @param y
     * @param z
     * @param power The power of the explosion. TNT has a power of 4.
     * @param doesDamage Whether or not this explosion deals damage.
     */
    public void explode(BaseEntity exploder, double x, double y, double z, float power, boolean doesDamage) {
        explode(exploder, x, y, z, power, false, doesDamage);
    }

    /**
     * Creates an explosion with the specified power at the given location.
     *
     * @param exploder The entity causing the explosion.
     * @param x
     * @param y
     * @param z
     * @param power The power of the explosion. TNT has a power of 4.
     * @param doesCauseFires Whether or not this explosion causes fires.
     * @param doesDamage Whether or not this explosion deals damage.
     */
    public void explode(BaseEntity exploder, double x, double y, double z, float power, boolean doesCauseFires, boolean doesDamage) {
        world.makeExplosion(exploder.getRecodeHandle(), x, y, z, power, doesDamage);
    }

    /**
     * Gets the random seed of the world.
     *
     * @return seed of the world
     */
    public long getRandomSeed() {
        return world.getWorldSeed();
    }

    /**
     * Sets a new light level with the specified level at the given location.
     *
     * @param x
     * @param y
     * @param z
     * @param newlevel The light level.
     */
    public void setLightLevel(int x, int y, int z, int newlevel) {
        world.setLightLevelOnBlockMap(x, y, z, newlevel);
    }

    /**
     * Gets the light level of the given location.
     *
     * @param x
     * @param y
     * @param z
     * @return Light level of the location.
     */
    public float getLightLevel(int x, int y, int z) {
        return world.getLightLevelAt(x, y, z);
    }

    /**
     * Updates the light around the given location.
     *
     * @param x
     * @param y
     * @param z
     */
    public void updateLight(int x, int y, int z) {
        for (int x2 = x - 2; x2 <= x + 2; x2++) {
            for (int y2 = y - 2; y2 <= y + 2; y2++) {
                for (int z2 = z - 2; z2 <= z + 2; z2++) {
                    // SRG this.getWorld().func_72966_v(x2, y2, z2);
                    this.getWorld().B(x2, y2, z2);
                }
            }
        }
    }

    public EntityTracker getEntityTracker() {
        return Wrapper.wrap(world.getEntityTracker());
    }

    public PlayerManager getPlayerManager() {
        return Wrapper.wrap(world.getPlayerManager());
    }

    public void removePlayerFromWorld(Player player) {
        world.removePlayerFromWorld(player.getRecodeHandle());
    }

    public void addPlayerToWorld(Player player) {
        world.addPlayerToWorld(player.getRecodeHandle());
    }

    /**
     * Gets this world's name.
     *
     * @return This world's name.
     */
    public String getName() {
        return world.getName();
    }

    /**
     * Get the default game mode for this world.
     *
     * @return The game mode for this world.
     */
    public int getGameMode() {
        return world.getGameMode().getId();
    }

    /**
     * Launch a projectile in this world.
     *
     * @param p The projectile to launch.
     */
    public void launchProjectile(Projectile p) {
        p.spawn();
    }

    /**
     * Have lightning strike a location.
     *
     * @param x The x coordinate to strike.
     * @param y The y coordinate to strike.
     * @param z The z coordinate to strike.
     */
    public void strikeLightning(double x, double y, double z) {
        world.makeLightningBolt(new Position(x, y, z));
    }

    /**
     * Have lightning strike a location.
     *
     * @param location The location to strike.
     */
    public void strikeLightning(Location location) {
        strikeLightning(location.x, location.y, location.z);
    }

    /**
     * Play a sound at the given location
     *
     * @param location location to play the sound
     * @param sound the sound to play
     * @param volume the volume to play the sound
     * @param pitch the pitch to play the sound at
     * @see Sound
     * @See Location
     */
    public void playSound(Location location, Sound sound, float volume, float pitch) {
        playSound(location.x, location.y, location.z, sound, volume, pitch);
    }

    /**
     * Play a sound at the given location
     *
     * @param x x value for the location
     * @param y y value for the location
     * @param z z value of the location
     * @param sound the sound to play
     * @param volume the volume to play the sound
     * @param pitch the pitch to play the sound at
     * @see Sound
     * @See Location
     */
    public void playSound(double x, double y, double z, Sound sound, float volume, float pitch) {
        SoundEffect.Type soundType = null;
        for (SoundEffect.Type type : SoundEffect.Type.values()) {
            if (type.getMcName().equals(sound.getSoundString())) {
                soundType = type;
                break;
            }
        }
        world.playSound(new SoundEffect(soundType, x, y, z, volume, pitch));
    }

    /**
     * Plays a note at a given location.
     *
     * @param location The location to play the note at.
     * @param instrument The instrument to play the note with.
     * @param pitch The pitch of the note (0-24?).
     */
    public void playNote(Location location, Sound.Instrument instrument, int pitch) {
        playNote(location.x, location.y, location.z, instrument, pitch);
    }

    /**
     * Plays a note at a given location.
     *
     * @param x The x coordinate to play the note at.
     * @param y The y coordinate to play the note at.
     * @param z The z coordinate to play the note at.
     * @param instrument The instrument to play the note with.
     * @param pitch The pitch of the note (0-24?).
     */
    public void playNote(double x, double y, double z, Sound.Instrument instrument, int pitch) {
        playNote((int) x, (int) y, (int) z, instrument, pitch);
    }

    /**
     * Plays a note at a given location.
     *
     * @param x The x coordinate to play the note at.
     * @param y The y coordinate to play the note at.
     * @param z The z coordinate to play the note at.
     * @param instrument The instrument to play the note with.
     * @param pitch The pitch of the note (0-24?).
     */
    public void playNote(int x, int y, int z, Sound.Instrument instrument, int pitch) {
        world.playNoteAt(x, y, z, instrument.ordinal(), (byte) pitch);
    }

    /**
     * Get the chunks that are currently loaded.
     *
     * @return The loaded chunks in Canary format.
     */
    public List<Chunk> getLoadedChunks() {
        List<net.canarymod.api.world.Chunk> recodeLoadedChunks = world.getLoadedChunks();
        List<Chunk> loadedChunks = new ArrayList<Chunk>(recodeLoadedChunks.size());
        for (net.canarymod.api.world.Chunk chunk : recodeLoadedChunks) {
            loadedChunks.add(Wrapper.wrap(chunk));
        }
        return loadedChunks;
    }

    @Override
    public String toString() {
        return "World{" + "world=" + world + '}';
    }

    public net.canarymod.api.world.World getRecodeHandle() {
        return world;
    }

    public static World[] fromRecodeArray(net.canarymod.api.world.World[] recode) {
        World[] classic = new World[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = Wrapper.wrap(recode[i]);
        }
        return classic;
    }

    public static net.canarymod.api.world.World[] toRecodeArray(World[] classic) {
        net.canarymod.api.world.World[] recode = new net.canarymod.api.world.World[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
