package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.entity.living.humanoid.CanaryHuman;
import net.canarymod.api.inventory.CanaryEnderChestInventory;
import com.minecraftonline.classicloader.classicapi.bridge.ContainerProxy;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.inventory.InventoryEnderChest;

/**
 * Interface for accessing the inventories of ender chests
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.inventory.EnderChestInventory.class)
public class EnderChestInventory extends ItemArray<ContainerProxy> {

    private final net.canarymod.api.inventory.EnderChestInventory inv;

    // These first two constructors are to maintain backward compatibility
    public EnderChestInventory(InventoryEnderChest container, Player owner) {
        this(container, (HumanEntity) owner);
    }

    public EnderChestInventory(net.minecraft.inventory.Container oContainer, InventoryEnderChest container, Player owner) {
        this(oContainer, container, (HumanEntity) owner);
    }

    public EnderChestInventory(InventoryEnderChest container, HumanEntity owner) {
        this(null, container, owner);
    }

    public EnderChestInventory(net.minecraft.inventory.Container oContainer, InventoryEnderChest container, HumanEntity owner) {
        super(oContainer, ContainerProxy.of(((CanaryHuman) owner.getRecodeHandle()).getHandle().getEnderChestInventory()));
        this.inv = ((CanaryHuman) owner.getRecodeHandle()).getHandle().getEnderChestInventory();
    }

    public EnderChestInventory(net.canarymod.api.inventory.EnderChestInventory eci) {
        super(null, ContainerProxy.of(eci));
        this.inv = eci;
    }

    /**
     * Returns an NBTTagList with data about the contents of this inventory.
     *
     * @return
     */
    public NBTTagList writeToTag() {
        // SRG return new NBTTagList(((CanaryEnderChestInventory) inv).getHandle().func_70487_g());
        return new NBTTagList(((CanaryEnderChestInventory) inv).getHandle().h());
    }

    /**
     * Sets this inventory's data to equal the contents of an NBTTagList.
     *
     * @param tag the tag to read data from
     */
    public void readFromTag(NBTTagList tag) {
        // SRG ((CanaryEnderChestInventory) inv).getHandle().func_70486_a(tag.getBaseTag());
        ((CanaryEnderChestInventory) inv).getHandle().a(tag.getBaseTag());
    }

    @Override
    public void update() {
        inv.update();
    }

    @Override
    public String getName() {
        return container.getName();
    }

    @Override
    public void setName(String value) {
        container.setName(value);
    }

    /**
     * Returns the player that this ender chest inventory belongs to.
     *
     * @return
     */
    public Player getPlayer() {
        return getHuman() instanceof Player ? (Player) getHuman() : null;
    }

    /**
     * Returns the {@link HumanEntity} that this ender chest inventory belongs
     * to.
     *
     * @return the {@link HumanEntity} that this ender chest inventory belongs
     * to
     */
    public HumanEntity getHuman() {
        return Wrapper.wrap(inv.getInventoryOwner());
    }

    public net.canarymod.api.inventory.EnderChestInventory getRecodeHandle() {
        return inv;
    }
}
