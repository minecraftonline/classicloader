package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.entity.living.LivingBase;
import net.canarymod.api.entity.living.animal.CanaryTameable;
import net.canarymod.api.entity.living.animal.Tameable;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.passive.EntityTameable;

@RecodeProxy(Tameable.class)
public class TamableEntity extends Mob {

    public TamableEntity(EntityTameable entity) {
        super(entity);
    }

    public TamableEntity(Tameable tamable) {
        super(tamable);
    }

    /**
     * If this animal is tame.
     *
     * @return True if tamed.
     */
    public boolean isTame() {
        return getRecodeHandle().isTamed();
    }

    /**
     * Sets the owner of this animal.
     *
     * @param player The new player who is the owner of this animal.
     */
    public void setOwner(Player player) {
        getRecodeHandle().setOwner(player.getRecodeHandle());
    }

    /**
     * Sets the owner of this animal.
     *
     * @param name The name of the player who owns this animal.
     */
    public void setOwnerName(String name) {
        getRecodeHandle().setOwner(name);
    }

    /**
     * Return the name of this animals owner.
     *
     * @return The name of the player who owns this animal.
     */
    public String getOwnerName() {
        return getRecodeHandle().getOwnerName();
    }

    /**
     * Returns a Player instance of this animals owner.
     *
     * @return A Player instance of this animals owner, else null.
     */
    public Player getOwner() {
        LivingBase owner = getRecodeHandle().getOwner();
        if (owner instanceof net.canarymod.api.entity.living.humanoid.Player) {
            return Wrapper.wrap((net.canarymod.api.entity.living.humanoid.Player) owner);
        }
        return null;
    }

    /**
     * Sets if this animal is tame.
     *
     * @param tame True if the animal should be tame.
     */
    public void setTame(boolean tame) {
        getRecodeHandle().setTamed(tame);
    }

    /**
     * Make this animal sit.
     *
     * @param sitting If this animal should be sitting.
     */
    public void setSitting(boolean sitting) {
        getRecodeHandle().setSitting(sitting);
    }

    /**
     * Returns if this animal is currently sitting.
     *
     * @return Sitting or not.
     */
    public boolean isSitting() {
        return getRecodeHandle().isSitting();
    }

    @Override
    public EntityTameable getEntity() {
        return (EntityTameable) super.getEntity();
    }

    @Override
    public Tameable getRecodeHandle() {
        return (Tameable) super.getRecodeHandle();
    }
}
