package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.entity.living.humanoid.CanaryVillager;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.passive.EntityVillager;

/**
 * Interface for villagers
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.entity.living.humanoid.Villager.class)
public class Villager extends Mob {

    /**
     * Represents the profession of the villager
     *
     * @author gregthegeek
     *
     */
    public enum Profession {

        FARMER,
        LIBRARIAN,
        PRIEST,
        BLACKSMITH,
        BUTCHER,
        GENERIC;
    }

    /**
     * Creates a villager wrapper
     *
     * @param oentity The entity to wrap
     */
    public Villager(EntityVillager oentity) {
        super(oentity);
    }

    /**
     * Creates a new villager
     *
     * @param world The world in which to create it
     */
    public Villager(World world) {
        super("Villager", world);
    }

    /**
     * Creates a new villager
     *
     * @param location The location at which to create it
     */
    public Villager(Location location) {
        super("Villager", location);
    }

    public Villager(net.canarymod.api.entity.living.humanoid.Villager villager) {
        super(villager);
    }

    /**
     * Returns the profession of the villager
     *
     * @return
     */
    public Profession getProfession() {
        return Profession.values()[getRecodeHandle().getProfession().getId()];
    }

    /**
     * Sets the profession of the villager
     *
     * @param profession
     */
    public void setProfession(Profession profession) {
        getRecodeHandle().setProfession(net.canarymod.api.entity.living.humanoid.Villager.Profession.fromId(profession.ordinal()));
    }

    @Override
    public EntityVillager getEntity() {
        return (EntityVillager) super.getEntity();
    }

    /**
     * Returns an immutable array of this villager's trades
     *
     * @return
     */
    public VillagerTrade[] getTrades() {
        return VillagerTrade.fromRecodeArray(getRecodeHandle().getTrades());
    }

    /**
     * Adds a trade to this villager
     *
     * @param trade
     */
    public void addTrade(VillagerTrade trade) {
        getRecodeHandle().addTrade(trade.getRecodeHandle());
    }

    /**
     * Removes a trade from this villager
     *
     * @param index the index of the trade to remove
     */
    public void removeTrade(int index) {
        getRecodeHandle().removeTrade(index);
    }

    @Override
    public net.canarymod.api.entity.living.humanoid.Villager getRecodeHandle() {
        return (net.canarymod.api.entity.living.humanoid.Villager) super.getRecodeHandle();
    }
}
