package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.entity.living.monster.CanaryCreeper;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for Creepers
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(net.canarymod.api.entity.living.monster.Creeper.class)
public class Creeper extends Mob {

    /**
     * Creates a creeper wrapper
     *
     * @param oentity The base entity to wrap
     */
    public Creeper(net.minecraft.entity.monster.EntityCreeper oentity) {
        super(oentity);
    }

    /**
     * Creates a new creeper
     *
     * @param world The world to create it in
     */
    public Creeper(World world) {
        super("Creeper", world);
    }

    /**
     * Creates a new creeper
     *
     * @param location The location at which to create it
     */
    public Creeper(Location location) {
        super("Creeper", location);
    }

    public Creeper(net.canarymod.api.entity.living.monster.Creeper creeper) {
        super(creeper);
    }

    /**
     * Returns whether or not this creeper is charged
     *
     * @return
     */
    public boolean isCharged() {
        return getRecodeHandle().isCharged();
    }

    /**
     * Sets whether or not this creeper is charged
     *
     * @param charged
     */
    public void setCharged(boolean charged) {
        getRecodeHandle().setCharged(charged);
    }

    @Override
    public net.minecraft.entity.monster.EntityCreeper getEntity() {
        return (net.minecraft.entity.monster.EntityCreeper) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.living.monster.Creeper getRecodeHandle() {
        return (net.canarymod.api.entity.living.monster.Creeper) super.getRecodeHandle();
    }
}
