package com.minecraftonline.classicloader.classicapi;

import net.canarymod.Canary;
import net.canarymod.api.nbt.DoubleTag;
import com.minecraftonline.classicloader.apt.RecodeProxy;

/**
 * Interface for the class net.minecraft.server.NBTTagDouble
 *
 * @author gregthegeek
 *
 */
@RecodeProxy(DoubleTag.class)
public class NBTTagDouble extends NBTBase {

    /**
     * Creates a wrapper around an net.minecraft.server.NBTTagDouble
     *
     * @param baseTag
     */
    public NBTTagDouble(net.minecraft.nbt.NBTTagDouble baseTag) {
        super(baseTag);
    }

    /**
     * Creates a new NBTTagDouble
     *
     * @param name the name of the new tag
     */
    public NBTTagDouble(String name) {
        this(name, 0);
    }

    /**
     * Creates a new NBTTagDouble
     *
     * @param name the name of the new tag
     * @param value the value of the new tag
     */
    public NBTTagDouble(String name, double value) {
        this(Canary.factory().getNBTFactory().newDoubleTag(value));
    }

    public NBTTagDouble(DoubleTag doubleTag) {
        super(doubleTag);
    }

    @Override
    public net.minecraft.nbt.NBTTagDouble getBaseTag() {
        return (net.minecraft.nbt.NBTTagDouble) super.getBaseTag();
    }

    /**
     * Returns the value of this NBTTagDouble
     *
     * @return
     */
    public double getValue() {
        return getRecodeHandle().getValue();
    }

    /**
     * Sets the value of this NBTTagDouble
     *
     * @param value the value to set
     */
    public void setValue(double value) {
        getRecodeHandle().setValue(value);
    }

    @Override
    public String toString() {
        return String.format("NBTTag[type=%s, name=%s, value=%f]", getTagName(getType()), getName(), getValue());
    }

    @Override
    public DoubleTag getRecodeHandle() {
        return (DoubleTag) super.getRecodeHandle();
    }

    public static NBTTagDouble[] fromRecodeArray(DoubleTag[] recode) {
        NBTTagDouble[] classic = new NBTTagDouble[recode.length];
        for (int i = 0; i < recode.length; i++) {
            classic[i] = new NBTTagDouble(recode[i]);
        }
        return classic;
    }

    public static DoubleTag[] toRecodeArray(NBTTagDouble[] classic) {
        DoubleTag[] recode = new DoubleTag[classic.length];
        for (int i = 0; i < classic.length; i++) {
            recode[i] = classic[i].getRecodeHandle();
        }
        return recode;
    }
}
