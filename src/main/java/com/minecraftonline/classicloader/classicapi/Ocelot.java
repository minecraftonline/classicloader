package com.minecraftonline.classicloader.classicapi;

import net.canarymod.api.entity.living.animal.CanaryOcelot;
import net.canarymod.api.entity.living.animal.Ocelot.SkinType;
import com.minecraftonline.classicloader.apt.RecodeProxy;
import net.minecraft.entity.passive.EntityOcelot;

@RecodeProxy(net.canarymod.api.entity.living.animal.Ocelot.class)
public class Ocelot extends TamableEntity {

    /**
     * Basic ocelot constructor.
     *
     * @param entity An instance of EntityOcelot to wrap around.
     */
    public Ocelot(EntityOcelot entity) {
        super(entity);
    }

    public Ocelot(net.canarymod.api.entity.living.animal.Ocelot ocelot) {
        super(ocelot);
    }

    /**
     * Set the skin of the ocelot.
     *
     * @param skin ID of the skin. 0 - Normal, 1 - Black, 2 - Orange, 3 -
     * Siamese, Any other value defaults to normal.
     */
    public void setSkin(int skin) {
        if (skin < 0 || skin > 3) {
            skin = 0;
        }
        getRecodeHandle().setSkinType(SkinType.values()[skin]);
    }

    /**
     * Get the id of this ocelots skin.
     *
     * @return 0, 1, 2 or 3. See {@link Ocelot#setSkin(int)} for skin ids.
     */
    public int getSkin() {
        return getRecodeHandle().getSkinType().ordinal();
    }

    @Override
    public EntityOcelot getEntity() {
        return (EntityOcelot) super.getEntity();
    }

    @Override
    public net.canarymod.api.entity.living.animal.Ocelot getRecodeHandle() {
        return (net.canarymod.api.entity.living.animal.Ocelot) super.getRecodeHandle();
    }
}
