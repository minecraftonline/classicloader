/*
 * Copyright (C) 2014 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.minecraftonline.classicloader.asm;

import com.google.common.io.Files;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.RemappingClassAdapter;
import org.objectweb.asm.commons.SimpleRemapper;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.util.CheckClassAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import com.minecraftonline.classicloader.ClassicLoader;
import org.objectweb.asm.Opcodes;

/**
 * This class transforms plugin classes' bytecode to be packaged and call packaged NMS classes.
 * @author Willem Mulder
 */
public class PluginClassTransformer {

    private static final Map<String, String> nmsMap = readMap("/nms.map");
    private static final Map<String, String> apiMap = readMap("/api.map");

    private static Map<String, String> readMap(String path) {
        InputStream in = PluginClassTransformer.class.getResourceAsStream(path);

        if (in != null) {
            HashMap<String, String> nameMap = new HashMap<String, String>();

            Scanner scan = new Scanner(in, "UTF-8");
            while (scan.hasNextLine()) {
                String[] splitLine = scan.nextLine().split("\t", 2);
                nameMap.put(splitLine[0], splitLine[1]);
            }

            return Collections.unmodifiableMap(nameMap);
        }
        return Collections.emptyMap();
    }

    public static byte[] transform(byte[] classBytes, final ClassLoader pluginClassLoader, String path) {

        ClassReader cr = new ClassReader(classBytes);
        ClassWriter cw = new ClassWriter(cr, ClassWriter.COMPUTE_FRAMES) {

            private ClassNode getSuperNode(ClassNode node) throws IOException {
                ClassReader superReader = new ClassReader(pluginClassLoader.getResourceAsStream(node.superName + ".class"));
                ClassNode superClass = new ClassNode();
                superReader.accept(superClass, 0);
                return superClass;
            }

            private boolean isAssignableFromASM(ClassNode assignee, ClassNode toAssign) throws IOException {
                if (assignee.name == null || toAssign.name == null) {
                    return false;
                } else if (assignee.superName == null) {
                    return true;
                } else {
                    while (toAssign.superName != null && !assignee.name.equals(toAssign.name)) {
                        toAssign = getSuperNode(toAssign);
                    }
                    return toAssign.superName != null;
                }
            }

            // Copied over and adapted to use our classLoader
            private String getCommonSuperClassASM(final String type1, final String type2) {
                try {
                    ClassReader cr1 = new ClassReader(pluginClassLoader.getResourceAsStream(type1 + ".class"));
                    ClassNode class1 = new ClassNode();
                    cr1.accept(class1, 0);

                    ClassReader cr2 = new ClassReader(pluginClassLoader.getResourceAsStream(type2 + ".class"));
                    ClassNode class2 = new ClassNode();
                    cr2.accept(class2, 0);

                    if (isAssignableFromASM(class1, class2)) {
                        return type1;
                    }
                    if (isAssignableFromASM(class2, class1)) {
                        return type2;
                    }
                    if ((cr1.getAccess() & Opcodes.ACC_INTERFACE) != 0 || (cr2.getAccess() & Opcodes.ACC_INTERFACE) != 0) {
                        return "java/lang/Object";
                    } else {
                        do {
                            class1 = getSuperNode(class1);
                        } while (!isAssignableFromASM(class1, class2));
                        return class1.name;
                    }
                } catch (IOException ex) {}
                return "java/lang/Object";
            }

            @Override
            protected String getCommonSuperClass(final String type1, final String type2) {
                Class<?> c, d;

                ClassLoader classLoader = getClass().getClassLoader();

                try {
                    c = Class.forName(type1.replace('/', '.'), false, classLoader);
                    d = Class.forName(type2.replace('/', '.'), false, classLoader);

                    if (c.isAssignableFrom(d)) {
                        return type1;
                    }
                    if (d.isAssignableFrom(c)) {
                        return type2;
                    }
                    if (c.isInterface() || d.isInterface()) {
                        return "java/lang/Object";
                    } else {
                        do {
                            c = c.getSuperclass();
                        } while (!c.isAssignableFrom(d));
                        return c.getName().replace('.', '/');
                    }
                } catch (ClassNotFoundException e) {
                    return getCommonSuperClassASM(type1, type2);
                }
            }
        };
        CheckClassAdapter cca;
        if (ClassicLoader.getInstance().DEBUG) {
            TraceClassVisitor tcv;
            try {
                File toPrint = new File("dump/" + path.replace(".class", ".asm"));
                toPrint.getParentFile().mkdirs();
                tcv = new TraceClassVisitor(cw, new PrintWriter(toPrint));
            } catch (FileNotFoundException e) {
                tcv = new TraceClassVisitor(cw, new PrintWriter(System.err));
            }
            cca = new CheckClassAdapter(tcv);
        } else {
            cca = new CheckClassAdapter(cw);
        }

        RemappingClassAdapter api;
        SimpleRemapper apiRemapper = new SimpleRemapper(apiMap);

        if (ClassicLoader.getInstance().LOAD_NMS) {
            RemappingClassAdapter nms = new RemappingClassAdapter(cca, new SimpleRemapper(nmsMap));
            api = new RemappingClassAdapter(nms, apiRemapper);
        } else {
            api = new RemappingClassAdapter(cca, apiRemapper);
        }

        cr.accept(api, ClassReader.SKIP_FRAMES);

        byte[] transformedClass = cw.toByteArray();
        if (ClassicLoader.getInstance().DUMP_CLASSES) {
            try {
                Files.write(transformedClass, new File("dump/" + path));
            } catch (IOException e) {
                // Meh.
            }
        }

        return transformedClass;
    }

}
