/*
 * Copyright (C) 2013 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.classicloader;

import com.minecraftonline.classicloader.classicapi.Colors;
import com.minecraftonline.classicloader.classicapi.Plugin;
import com.minecraftonline.classicloader.classicapi.etc;
import java.util.ArrayList;
import java.util.List;
import net.canarymod.Canary;
import net.canarymod.api.Server;
import net.canarymod.chat.MessageReceiver;
import net.canarymod.commandsys.Command;
import net.canarymod.commandsys.CommandListener;
import net.canarymod.commandsys.TabComplete;
import net.canarymod.commandsys.TabCompleteHelper;
import net.visualillusionsent.utils.SystemUtils;

/**
 * Commands!
 */
public class Commands implements CommandListener {
    private static final boolean supportsColour;
    private static final String info = String.format("\u00a7e%s\u00a7r version \u00a7b%s\u00a7r by \u00a7a%s\u00a7r",
                                                     Commands.class.getPackage().getImplementationTitle(),
                                                     Commands.class.getPackage().getImplementationVersion(),
                                                     Commands.class.getPackage().getImplementationVendor());

    static {
        // Start fancy colour detection!
        boolean colour = false;
        if (SystemUtils.isUnix() || SystemUtils.isMac()) {
            java.io.File tputFile = new java.io.File("/usr/bin/tput");
            if (tputFile.canExecute()) {
                try {
                    Process tput = Runtime.getRuntime().exec(new String[]{tputFile.getAbsolutePath(), "setaf", "1"});
                    tput.waitFor();
                    colour = tput.exitValue() == 0;
                } catch (java.io.IOException ignored) {
                } catch (InterruptedException ignored) {}
            }
        }

        supportsColour = colour;
    }

    @Command(aliases = {"cl", "classicloader"},
            description = "ClassicLoader base command",
            helpLookup = "cl",
            permissions = "com.minecraftonline.classicloader",
            toolTip = "Usage: /cl [help|info|enable <plugin>|disable <plugin>|reload <plugin>]")
    public void cl(MessageReceiver caller, String[] args) {
        Canary.help().getHelp(caller, "cl");
    }

    @TabComplete(commands = "cl")
    public List<String> tabCL(MessageReceiver caller, String[] args) {
        if (args.length == 1) {
            return TabCompleteHelper.matchTo(args, new String[] {"help", "info", "enable", "disable", "reload"});
        } else if (args.length == 2 && args[1].equals("disable") || args[1].equals("reload")) {
            return tabEnabledPlugin(caller, args);
        } else {
            return null;
        }
    }

    @Command(aliases = "enable",
            parent = "cl",
            min = 2,
            max = 2,
            description = "Enable or load a Classic plugin",
            helpLookup = "cl.enable",
            permissions = "com.minecraftonline.classicloader.enable",
            toolTip = "Usage: /cl enable <plugin>")
    public void enablePlugin(MessageReceiver caller, String[] args) {
        String message;
        if (etc.getLoader().enablePlugin(args[1])) {
            message(caller, Colors.LightGreen + args[1] + " successfully loaded");
        } else {
            message(caller, Colors.Rose + String.format("Could not load %s, does it exist?", args[1]));
            message(caller, Colors.Rose + String.format("(Check your capitalization and/or server log)"));
        }
    }

    @Command(aliases = "disable",
            parent = "cl",
            min = 2,
            max = 2,
            description = "Disable a Classic plugin",
            helpLookup = "cl.disable",
            permissions = "com.minecraftonline.classicloader.disable",
            toolTip = "Usage: /cl disable <plugin>")
    public void disablePlugin(MessageReceiver caller, String[] args) {
        etc.getLoader().disablePlugin(args[1]);
        message(caller, Colors.LightGreen + args[1] + " disabled.");
    }

    @Command(aliases = "reload",
            parent = "cl",
            min = 2,
            max = 2,
            description = "Reload a Classic plugin",
            helpLookup = "cl.reload",
            permissions = "com.minecraftonline.classicloader.reload",
            toolTip = "Usage: /cl reload <plugin>")
    public void reloadPlugin(MessageReceiver caller, String[] args) {
        if (etc.getLoader().reloadPlugin(args[1])) {
            message(caller, Colors.LightGreen + args[1] + " successfully reloaded");
        } else {
            message(caller, Colors.Rose + "Could not reload " + args[1]);
            message(caller, Colors.Rose + "(Check your capitalization and/or server log)");
        }
    }

    public List<String> tabEnabledPlugin(MessageReceiver caller, String[] args) {
        List<Plugin> plugins = etc.getLoader().getPlugins();
        List<String> completions = new ArrayList<String>(plugins.size());
        String toComplete = args[args.length - 1];
        for (Plugin plugin : plugins) {
            if (TabCompleteHelper.startsWith(toComplete, plugin.getName())) {
                completions.add(plugin.getName());
            }
        }

        return completions;
    }

    @Command(aliases = "info",
            parent = "cl",
            description = "Information about ClassicLoader",
            helpLookup = "cl.info",
            permissions =  "com.minecraftonline.classicloader.info",
            toolTip = "Usage: /cl info")
    public void info(MessageReceiver caller, String[] args) {
        message(caller, info);
    }

    @Command(aliases = "list",
            parent = "cl",
            description = "List Classic plugins",
            helpLookup = "cl.list",
            permissions = "com.minecraftonline.classicloader.list",
            toolTip = "Usage: /cl list")
    public void list(MessageReceiver caller, String[] args) {
        message(caller, "\u00a7ePlugin list:\u00a7r " + etc.getLoader().getPluginList());
    }

    private static void message(MessageReceiver caller, String message) {
        if (caller instanceof Server) {
            if (supportsColour) {
                caller.message(Colors.toANSI(message));
            } else {
                caller.message(Colors.strip(message));
            }
        } else {
            caller.message(message);
        }
    }
}
