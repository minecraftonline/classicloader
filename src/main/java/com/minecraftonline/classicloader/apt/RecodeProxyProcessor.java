/*
 * Copyright (C) 2013 Willem Mulder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.minecraftonline.classicloader.apt;

import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import javax.tools.JavaFileObject;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import static javax.lang.model.SourceVersion.RELEASE_6;
import static javax.lang.model.element.ElementKind.*;
import static javax.lang.model.element.Modifier.PUBLIC;
import static javax.lang.model.element.Modifier.STATIC;
import javax.tools.Diagnostic;
import static javax.tools.Diagnostic.Kind.*;

/**
 * Annotation processor for generating Wrapper and checking RecodeProxy-annotated classes.
 * @author Willem Mulder
 */
@SupportedSourceVersion(RELEASE_6)
@SupportedAnnotationTypes("com.minecraftonline.classicloader.apt.RecodeProxy")
public class RecodeProxyProcessor extends AbstractProcessor {

    private Types typeUtils;
    private Map<String, String> classMap = new HashMap<String, String>();

    @Override
    public void init(ProcessingEnvironment procEnv) {
        super.init(procEnv);
        typeUtils = procEnv.getTypeUtils();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for (Element annotated : roundEnv.getElementsAnnotatedWith(RecodeProxy.class)) {
            boolean foundToArray = false, foundFromArray = false,
                    foundConstructor = false, foundRecodeGetter = false;
            TypeElement annotatedClass = (TypeElement) annotated;

            TypeMirror recodeType = null;
            try {
                annotated.getAnnotation(RecodeProxy.class).value();
            } catch (MirroredTypeException mte) {
                recodeType = mte.getTypeMirror();
            }

            if (recodeType == null) {
                processingEnv.getMessager().printMessage(ERROR, "RecodeProxy value is null!");
                return true;
            }

            String fqRecodeName = ((TypeElement) typeUtils.asElement(recodeType)).getQualifiedName().toString();
            if (!(classMap.containsKey(fqRecodeName) && !typeUtils.isSubtype(processingEnv.getElementUtils().getTypeElement(
                    classMap.get(fqRecodeName)).asType(), annotatedClass.asType()))) {
                classMap.put(fqRecodeName, annotatedClass.getQualifiedName().toString());
            }

            for (Element enclosed : annotated.getEnclosedElements()) {
                if (foundToArray && foundFromArray && foundConstructor && foundRecodeGetter) {
                    break;
                }
                if (enclosed.getKind() == METHOD) {
                    ExecutableElement ee = (ExecutableElement) enclosed;
                    if (ee.getModifiers().contains(STATIC)) {
                        foundFromArray = foundFromArray || ee.getSimpleName().contentEquals("fromRecodeArray")
                                && ee.getReturnType().equals(typeUtils.getArrayType(annotated.asType()))
                                && ee.getParameters().equals(Collections.singletonList(
                                        typeUtils.getArrayType(recodeType)));
                        foundToArray = foundToArray || ee.getSimpleName().contentEquals("toRecodeArray")
                                && ee.getReturnType().equals(typeUtils.getArrayType(recodeType))
                                && ee.getParameters().equals(Collections.singletonList(
                                        typeUtils.getArrayType(annotated.asType())));
                    } else {
                        foundRecodeGetter = foundRecodeGetter || ee.getSimpleName().contentEquals("getRecodeHandle")
                                && ee.getReturnType().equals(recodeType) && ee.getParameters().equals(Collections.emptyList())
                                && ee.getModifiers().contains(PUBLIC);
                    }
                } else if (enclosed.getKind() == CONSTRUCTOR && !foundConstructor) {
                    ExecutableElement ee = (ExecutableElement) enclosed;
                    List<? extends VariableElement> list = ee.getParameters();
                    if (list.size() == 1) {
                        VariableElement ve = list.get(0);
                        foundConstructor = ve.asType().equals(recodeType) && ee.getModifiers().contains(PUBLIC);
                    }
                }
            }
            if (!foundToArray) {
                processingEnv.getMessager().printMessage(NOTE, "RecodeProxy-annotated element does not implement toRecodeArray", annotated, annotated.getAnnotationMirrors().get(0));
            }
            if (!foundFromArray) {
                processingEnv.getMessager().printMessage(NOTE, "RecodeProxy-annotated element does not implement fromRecodeArray", annotated, annotated.getAnnotationMirrors().get(0));
            }
            if (!foundConstructor) {
                processingEnv.getMessager().printMessage(ERROR, "RecodeProxy-annotated element does not implement the recode element constructor", annotated, annotated.getAnnotationMirrors().get(0));
            }
            if (!foundRecodeGetter) {
                processingEnv.getMessager().printMessage(ERROR, "RecodeProxy-annotated element does not implement getRecodeHandle", annotated, annotated.getAnnotationMirrors().get(0));
            }
        }

        if (roundEnv.processingOver()) {
            try {
                Properties props = new Properties();
                URL url = this.getClass().getClassLoader().getResource("velocity.properties");
                props.load(url.openStream());

                VelocityEngine ve = new VelocityEngine(props);
                ve.init();

                VelocityContext vc = new VelocityContext();
                vc.put("recodeMap", classMap);

                Template vt = ve.getTemplate("com/minecraftonline/classicloader/classicapi/Wrapper.java.vm", "UTF-8");

                JavaFileObject jfo = processingEnv.getFiler().createSourceFile("com.minecraftonline.classicloader.classicapi.Wrapper");
                Writer writer = jfo.openWriter();

                vt.merge(vc, writer);

                writer.close();
            } catch (IOException ex) {
                processingEnv.getMessager().printMessage(ERROR, "IOException while loading velocity.properties: " + ex.getMessage());
            }
        }

        return true;
    }
}
